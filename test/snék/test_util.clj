(ns snék.test-util
  (:refer-clojure :exclude (+ - * / zero?))
  (:use [clojure.algo.generic.arithmetic :only (+ - * /)]
        [clojure.algo.generic.comparison :only (zero?)]
        [clojure.algo.generic.math-functions :only (abs approx= sqrt)])
  (:require [clojure.algo.generic.math-functions :refer (approx=)]))

(defmacro show [& exprs]
  `(do
     (println "+----")
     ~@(for [expr exprs]
         `(println "| " ~(str expr) "=" ~expr))
     (println "+----")))

(defn approx?
  "Like `algo.generic.math-functions/approx=`, but with a default value (1e-10)
  for the tolerance."
  ([x y atol] (approx= x y atol))
  ([x y] (approx= x y 1e-10)))

(defn approx-modulo?
  "Returns true if `x - y` is approximately an integer multiple of `m`.
  Modulus must be positive."
  [x y m & {:keys [atol] :or {atol 1e-10}}]
  (assert (pos? m))
  (let [r (mod (- x y) m)]
    (or (approx= 0 r atol)
        (approx= m r atol)
        (approx= (- m) r atol))))

(defn all-approx=
  "Predicate that returns true if each element of `xs` is `approx=` to the corresponding element of `ys`."
  [xs ys atol]
  (every? true? (map #(approx= %1 %2 atol)
                     xs ys)))

(defn approx-perp-2d?
  "Returns true if v and w are approximately perpendicular vectors, i.e., their
  dot product is `approx=` to 0."
  [[x1 y1 :as v]
   [x2 y2 :as w]
   atol]
  (approx= (+ (* x1 x2) (* y1 y2))
           0 atol))

(defn all-approx?
  "Returns true if either `xs` is an empty sequence, or every element in `xs`
  is approximately equal (with absolute tolerance `atol`) to the first
  element."
  [xs & {:keys [atol] :or {atol 1e-10}}]
  (or
    (not (seq xs))
    (every? #(approx= % (first xs) atol) xs)))

(defn proj-equiv?
  "Predicate that returns true if vectors `v` and `w` are nonzero-scalar
  multiples of each other.

  Here `v` and `w` are assumed to have the same length.  If either `v` or `w`
  is all zeros, the result is unspecified."
  [v w]
  (and (every? true? (map =
                          (map zero? v)
                          (map zero? w)))
       (all-approx? (for [[x y] (map vector v w)
                          :when (not (zero? x))]
                      (/ y x)))))


(ns snék.game-test
  (:require [snék.core :as c :refer :all]
            [snék.core-util :refer :all]
            [snék.game :as g]
            [snék.mobius :as m]
            [snék.test-util :refer :all]
            [clojure.algo.generic.math-functions :refer (approx=)]
            [clojure.test :refer [testing is deftest]]))

(deftest neck
  (testing "invariants of the neck that 1p view is based on"
    (let [{::c/keys [line-angle-min
                     line-angle-max
                     start-angle
                     delta-angle]
           :as snake-neck}  (g/snake-neck-1p-at-bearing 2.345)
          [bwd fwd] (h-line-end-pts-bwd-fwd snake-neck)]
      (is (approx= (normsq bwd)
                   1 1e-4))
      (is (approx= (normsq fwd)
                   1 1e-4))
      (is (< line-angle-min
             start-angle
             line-angle-max
             (+ line-angle-min Math/PI)))
      (is (< line-angle-min
             (+ start-angle delta-angle)
             line-angle-max))
      (is (< (normsq (arc-start-pt snake-neck))
             1))
      (is (< (normsq (arc-end-pt snake-neck))
             1)))))


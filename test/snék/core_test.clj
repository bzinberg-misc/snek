(ns snék.core-test
  (:require [snék.core :as c :refer :all]
            [snék.core-util :refer :all]
            [snék.test-util :refer :all]
            [clojure.algo.generic.math-functions :refer (approx=)]
            [clojure.test :refer [testing is deftest]]))

(deftest test-residue-just-above
  (testing "`residue-just-above` on edge cases and near-edge cases"
    (is (= -2.3
           (residue-just-above -2.3 -2.3 0.5))
        "When x = minval, result should be x")
    (is (approx? 2.3001
                 (residue-just-above 2.3001 2.3 0.5))
        "x slightly greater than minval, positive case")
    (is (approx? -2.2999
                 (residue-just-above -2.2999 -2.3 0.5))
        "x slightly greater than minval, negative case")
    (is (approx? 2.7999
                 (residue-just-above 2.2999 2.3 0.5))
        "x slightly less than minval, positive case")
    (is (approx? -1.8001
                 (residue-just-above -2.3001 -2.3 0.5))
        "x slightly less than minval, negative case")
    (is (approx? 2.3001
                 (residue-just-above 3.8001 2.3 0.5))
        "x slightly greater than minval + some-integer * modulus")
    (is (approx? 2.7999
                 (residue-just-above 3.7999 2.3 0.5))
        "x slightly less than minval + some-integer * modulus")))

(deftest test-h-line
  (testing "`h-line` produces an arc perpendicular to the disk"
    (doseq
      [ctr [
            ; Test a point in each quadrant, to exercise the case when the arc
            ; straddles the angle wrap-around value
            [ 3.3  0.6]
            [-3.3  0.6]
            [-3.3 -0.6]
            [ 3.3 -0.6]
            ]]
      (let
        [{::c/keys [radius
                    line-angle-min
                    line-angle-max]} (c/h-line ctr)
         pt-on-line (fn [angle] (point-on-circle ctr radius angle))
         intersection-low (pt-on-line line-angle-min)
         intersection-high (pt-on-line line-angle-max)
         rvector-low (-v intersection-low ctr)
         rvector-high (-v intersection-high ctr)]
        (is (<= 0
                (- line-angle-max line-angle-min)
                Math/PI))
        (doseq [angle [line-angle-min line-angle-max]]
          (let [intersection (pt-on-line angle)
                rvector (-v intersection ctr)]
            (is (approx? 1 (normsq intersection))
                "(Supposed) intersection point is on the disk boundary")
            (is (approx? 0 (dot rvector intersection))
                "At intersection point, radius of the arc is perpendicular to
                radius of the disk")))))))

(deftest test-seg-from-start-length-basic
  (testing "`seg-from-start-length` produces a segment of the right length"
    (doseq [ctr [[ 2.  1.]
                 [-2.  1.]
                 [-2. -1.]
                 [ 2. -1.]]
            angle-dir [:pos :neg]]
      (let [{::c/keys [radius
                       line-angle-min
                       line-angle-max]} (c/h-line ctr)
            pt-on-line (fn [angle] (point-on-circle ctr radius angle))
            P (pt-on-line (+ (* 0.7 line-angle-min)
                             (* 0.3 line-angle-max)))
            A (pt-on-line line-angle-min)
            B (pt-on-line line-angle-max)
            [x y] (-v P ctr)
            desired-h-length 3
            seg (c/seg-from-start-length {::c/center ctr
                                          ::c/radius (norm [x y])
                                          ::c/start-angle (Math/atan2 y x)
                                          ::c/h-length desired-h-length}
                                         ::c/angle-dir angle-dir)
            Q (pt-on-line
                (+ (::c/start-angle seg)
                   (::c/delta-angle seg)))]
        (is (approx? desired-h-length
                     (Math/abs
                       (Math/log
                         (/ (* (e-dist A Q) (e-dist B P))
                            (* (e-dist A P) (e-dist B Q))))))
            "With hard-coded length computation")
        (is (approx? desired-h-length
                     (h-length-of seg))
            "With `h-length-of`")
        (is (approx? desired-h-length
                     (h-dist P Q))
            "With direct, line-free `h-dist` computation")))))

(deftest test-seg-from-start-length-negative
  (testing "`seg-from-start-length`: negative direction produces an angle
           smaller than positive direction"
    (doseq [ctr [[ 2.  1.]
                 [-2.  1.]
                 [-2. -1.]
                 [ 2. -1.]]]
      (let [{::c/keys [radius
                       line-angle-min
                       line-angle-max]} (c/h-line ctr)
            start-angle (+ (* 0.7 line-angle-min)
                           (* 0.3 line-angle-max))
            desired-h-length 3
            [delta-angle-pos
             delta-angle-neg] (for [angle-dir [:pos :neg]]
                                (::c/delta-angle
                                  (c/seg-from-start-length
                                    {::c/center ctr
                                     ::c/radius radius
                                     ::c/start-angle start-angle
                                     ::c/h-length desired-h-length}
                                    ::c/angle-dir angle-dir)))]
        (is (< line-angle-min
               (+ start-angle delta-angle-neg)
               (+ start-angle delta-angle-pos)
               line-angle-max))))))

(deftest test-center-from-point-r-dir
  (testing "`center-from-point-r-dir`: the hyperbolic line with the returned
           hyperbolic center does contain the point"
    (doseq [pt [[ 0.12  0.34]
                [-0.12  0.34]
                [-0.12 -0.34]
                [ 0.12 -0.34]]
            r-dir [[ 0.6  0.7]
                   [-0.6  0.7]
                   [-0.6 -0.7]
                   [ 0.6 -0.7]]]
      (let [center (c/center-from-point-r-dir
                     pt r-dir)
            center-to-pt (-v pt center)
            {line-radius ::c/radius} (c/h-line center)]
        (is (approx? line-radius
                     (norm center-to-pt)))))))

(deftest test-h->e-circle
  (testing "`h->e-circle`"
    (let [h-center [-0.1 -0.2]
          h-radius 2
          [e-center e-radius] (c/h->e-circle h-center h-radius)
          θ (angle-of h-center)
          A (cossin (+ Math/PI θ))
          P (point-on-circle e-center e-radius (+ Math/PI θ))
          Q (point-on-circle e-center e-radius θ)
          B (cossin θ)]
      (is (pos? e-radius))
      (is (< (+ (norm e-center) e-radius)
             1)
          "Circle lies entirely within the disk")
      (is (approx? h-radius
                   (Math/log (/ (* (e-dist A h-center) (e-dist B P))
                                (* (e-dist A P) (e-dist B h-center)))))
          "Hyperbolic distance from h-center to P is h-radius")
      (is (approx? h-radius
                   (Math/log (/ (* (e-dist A Q) (e-dist B h-center))
                                (* (e-dist A h-center) (e-dist B Q)))))
          "Hyperbolic distance from h-center to Q is h-radius"))))

(deftest test-arc-intersections
  (testing "`arc-intersections`"
    (doseq [l-center-to-center [; center-to-center is smaller than r1
                                0.1
                                0.3
                                ; center-to-center is bigger than r1
                                1.3
                                3.8]
            θ [0.82  ; first quadrant
               1.5
               3.1   ; second quadrant
               3.2   ; third qudarant
               4.9   ; fourth quadrant
               6.2]
            ;; Test with both positive and negative `delta-angle` on each arc
            sign1 [1 -1]
            sign2 [1 -1]]
      (let [ctr1 [-1.1 0.5]
            r1 0.6
            α 0.15
            intersection-low (point-on-circle ctr1 r1 (- θ α))
            intersection-high (point-on-circle ctr1 r1 (+ θ α))
            ctr2 (point-on-circle ctr1
                                  l-center-to-center
                                  θ)
            r2 (e-dist ctr2 intersection-low)
            β (mod (- (angle-of (-v intersection-low ctr2))
                      θ)
                   Math/PI)
            the-arc-intersections (fn [arc1-start-angle arc1-delta-angle
                                       arc2-start-angle arc2-delta-angle]
                                    (c/arc-intersections
                                      {::c/center ctr1
                                       ::c/radius r1
                                       ::c/start-angle arc1-start-angle
                                       ::c/delta-angle arc1-delta-angle}
                                      {::c/center ctr2
                                       ::c/radius r2
                                       ::c/start-angle arc2-start-angle
                                       ::c/delta-angle arc2-delta-angle}))]

        (assert (not-every? true? (map approx? ctr1 ctr2))
                "Correctness of the test construction: I meant to have the
                centers be distinct")
        (assert (and (approx? r1 (e-dist ctr1 intersection-low))
                     (approx? r1 (e-dist ctr1 intersection-high))
                     (approx? r2 (e-dist ctr2 intersection-low))
                     (approx? r2 (e-dist ctr2 intersection-high)))
                "Correctness of the test construction: `intersection-low` and
                `intersection-high` are both intersection points of the two
                circles (and thus are the only two intersection points, if
                they're distinct)")

        ;; Both arcs are small, and they intersect only at `intersection-high`
        (let [expected [(+ θ α)]
              actual (the-arc-intersections (+ θ α (* -0.01 sign1))
                                            (* 0.02 sign1)
                                            (+ Math/PI θ (- β) (* -0.05 sign2))
                                            (* 0.07 sign2))]
          (is (= (count expected) (count actual)))
          (is (every? true? (map approx-modulo? expected actual (repeat _2pi)))))

        ;; Both arcs contain both intersection points
        (let [expected [(- θ α) (+ θ α)]
              actual (the-arc-intersections 0
                                            (* _2pi sign1)
                                            0
                                            (* _2pi sign2))]
          (is (= (count expected) (count actual)))
          (is (every? true? (map approx-modulo? expected actual (repeat _2pi)))))

        ;; arc1 contains both intersection points, arc2 contains just one
        ;; intersection point
        (let [expected [(- θ α)]
              actual (the-arc-intersections 0
                                            (* _2pi sign1)
                                            (+ Math/PI θ β (* -0.05 sign2))
                                            (* 0.07 sign2))]
          (is (= (count expected) (count actual)))
          (is (every? true? (map approx-modulo? expected actual (repeat _2pi)))))

        ;; arc2 contains both intersection points, arc1 contains just one
        ;; intersection point
        (let [expected [(+ θ α)]
              actual (the-arc-intersections (+ θ α (* -0.01 sign1))
                                            (* 0.02 sign1)
                                            0
                                            (* _2pi sign2))]
          (is (= (count expected) (count actual)))
          (is (every? true? (map approx-modulo? expected actual (repeat _2pi)))))

        (is (empty? (the-arc-intersections (+ θ α (* 0.1 sign1))
                                           (* 0.02 sign1)
                                           (+ Math/PI θ (- β) (* -0.05 sign2))
                                           (* 0.07 sign2)))
            "No intersection when arc2 contains the intersection point but arc1
            is just a little ahead of it")

        (is (empty? (the-arc-intersections (+ θ α (* -0.021 sign1))
                                           (* 0.02 sign1)
                                           (+ Math/PI θ (- β) (* -0.05 sign2))
                                           (* 0.07 sign2)))
            "No intersection when arc2 contains the intersection point but arc1
            is just a little behind it")

        (is (empty? (the-arc-intersections (+ θ α (* -0.01 sign1))
                                           (* 0.02 sign1)
                                           (+ Math/PI θ (- β) (* 0.05 sign2))
                                           (* 0.07 sign2)))
            "No intersection when arc1 contains the intersection point but arc2
            is just a little ahead of it")

        (is (empty? (the-arc-intersections (+ θ α (* -0.01 sign1))
                                           (* 0.02 sign1)
                                           (+ Math/PI θ (- β) (* -0.071 sign2))
                                           (* 0.07 sign2)))
            "No intersection when arc1 contains the intersection point but arc2
            is just a little behind it")

        (is (empty? (the-arc-intersections (+ θ α (* -0.01 sign1))
                                           (* 0.02 sign1)
                                           (+ Math/PI θ β (* -0.05 sign2))
                                           (* 0.07 sign2)))
            "No intersection when the arcs contain only different intersection
            points")))))

(deftest test-mat-ops-2x2
  (let [A [1 2
           3 4]
        x [5 6]
        Ax [17 39]]
    (testing "matvecmul-2x2"
      (is (all-approx= (matvecmul-2x2 A x)
                       Ax 1e-4)))
    (testing "matvecdiv-2x2"
      (is (all-approx= (matvecdiv-2x2 A Ax)
                       x 1e-4)))))

(deftest test-circum-center
  (testing "`circum-center`"
    (let [A [1. 2.]
          B [3. 4.]
          C [-5. 6.]
          ctr (c/circum-center A B C)]
      (dorun
        (for [[pt1 pt2] [[A B]
                         [B C]
                         [A C]]]
          (is (approx= (e-dist ctr pt1)
                       (e-dist ctr pt2) 1e-4)
              "Vertices of triangle are equidistant from circumcenter"))))))

(deftest endpoints-bwd-fwd
  (testing "rerep and order line angle extremes"
    (let [angle1 (+ 1.23 (* 4 Math/PI))
          angle2 (+ 2.23 (* -8 Math/PI))
          [a1 a2] (rerep-and-order-line-angle-extremes
                    angle1 angle2)
          [b1 b2] (rerep-and-order-line-angle-extremes
                    angle2 angle1)]
      (is (approx-modulo? a1 angle1 _2pi))
      (is (approx-modulo? a2 angle2 _2pi))
      (is (< 0 (- a2 a1) Math/PI))

      ; b1 should still match with angle1, even though angle1 was the second
      ; arg
      (is (approx-modulo? b1 angle1 _2pi))
      (is (approx-modulo? b2 angle2 _2pi))
      (is (< 0 (- b2 b1) Math/PI))))
  
  (testing "h-line-end-pts-bwd-fwd"
    (let [ctr [(Math/sqrt 2.) (- (Math/sqrt 2.))]
          seg1 (-> (h-line ctr)
                   (assoc ::c/start-angle (* 3/4 Math/PI)
                          ::c/delta-angle 0.1)
                   (#(assoc % ::c/h-length (h-length-of %))))
          [bwd1 fwd1] (h-line-end-pts-bwd-fwd seg1)
          seg2 (-> seg1
                   (update ::c/delta-angle -)
                   (#(assoc % ::c/h-length (h-length-of %))))
          [bwd2 fwd2] (h-line-end-pts-bwd-fwd seg2)]
      (dorun (for [[bwd fwd] [[bwd1 fwd1]
                              [bwd2 fwd2]]]
               (do
                 (is (approx= (normsq bwd) 1. 1e-4)
                     "bwd is on the disk boundary")
                 (is (approx= (normsq fwd) 1. 1e-4)
                     "fwd is on the disk boundary")
                 (is (approx-perp-2d? (-v ctr bwd) bwd 1e-4)
                     "Arc is perpendicular to the disk boundary at bwd")
                 (is (approx-perp-2d? (-v ctr fwd) fwd 1e-4)
                     "Arc is perpendicular to the disk boundary at fwd"))))
      (is (and (all-approx= bwd1 fwd2 1e-4)
               (all-approx= bwd2 fwd1 1e-4))
          "Sign change of delta-angle produces swap of bwd and fwd"))))


(ns snék.mobius-test
  (:refer-clojure :exclude (+ - * / zero?))
  (:use [clojure.algo.generic.arithmetic :only (+ - * /)]
        [clojure.algo.generic.comparison :only (zero?)]
        [clojure.algo.generic.math-functions :only (abs approx= sqrt)])
  (:require [snék.core :as c :refer :all]
            [snék.core-util :refer :all]
            [snék.game :as g]
            [snék.mobius :as m]
            [snék.test-util :refer :all]
            [clojure.test :refer [testing is deftest]]))

(deftest affine-proj-round-trip
  (testing "affine -> proj -> affine round trip"
    (let [zs [(m/complex 1.2 -3.4)
              (m/complex 0 0)]]
      (dorun (for [z zs]
               (is (approx= (m/proj->affine (m/affine->proj z))
                            z 1e-4))))))
  (testing "proj -> affine -> proj round trip"
    (let [zs [[(m/complex 1.2 -3.4) (m/complex -5.6 7.8)]
              [(m/complex 0 0) (m/complex 1 0)]
              ; point at infinity
              [(m/complex 1 0) (m/complex 0 0)]]]
      (dorun (for [z zs]
               (is (proj-equiv? (m/affine->proj (m/proj->affine z))
                                z)))))))

(deftest test-matmul
  (testing "Spot-check the result of matmul"
    (let [A [1 2
             3 4]
          B [5 6
             7 8]
          AB (m/proj-matmul-2x2 A B)
          expected-proj-result-1 [19 22
                                  43 50]
          expected-proj-result-2 [38 44
                                  86 100]
          unexpected-proj-result [10 11
                                  12 13]]

      (is (= 4 (count AB)))

      (is (proj-equiv? AB expected-proj-result-1))

      (is (proj-equiv? AB expected-proj-result-2))

      (is (not (proj-equiv? AB unexpected-proj-result)))))

  (testing "Spot-check associativity of matmul and matvecmul"
    (let [A [1 2
             3 4]
          B [5 6
             7 8]
          v [9 10]]
      (is (proj-equiv?
            (m/proj-matvecmul-2x2 (m/proj-matmul-2x2 A B)
                                  v)
            (m/proj-matvecmul-2x2 A
                                  (m/proj-matvecmul-2x2 B v)))))))

(deftest test-inverse
  (testing "Spot-check inverses"
    (let [A [1 2
             3 4]]
      (is (proj-equiv?
            (m/proj-matmul-2x2 A
                               (m/proj-inv-2x2 A))
            [1 0
             0 1])))))

(deftest test-matvecmul
  (testing "Spot-check the result of matvecmul"
    (let [A [1 2
             3 4]
          v [5 6]]
      (is (proj-equiv?
            (m/proj-matvecmul-2x2 A v)
            [17 39])))))

(deftest test-mobius-by-0-1-inf
  (testing "Spot-check `mobius-by-0-1-inf`"
    (let [z1 (m/complex 1.2 3.4)
          z2 (m/complex 5.6 7.8)
          z3 (m/complex 9.1 2.3)
          A (m/mobius-by-0-1-inf z1 z2 z3)]
      (is (proj-equiv? (m/proj-matvecmul-2x2 A (m/affine->proj z1))
                       (m/affine->proj (m/to-complex 0))))
      (is (proj-equiv? (m/proj-matvecmul-2x2 A (m/affine->proj z2))
                       (m/affine->proj (m/to-complex 1))))
      (is (proj-equiv? (m/proj-matvecmul-2x2 A (m/affine->proj z3))
                       (m/affine->proj Double/POSITIVE_INFINITY))))))

(deftest test-mobius-by-3-pts
  (testing "Spot-check `mobius-by-3-pts`"
    (let [z1 (m/complex 1.2 3.4)
          z2 (m/complex 5.6 7.8)
          z3 (m/complex -2.3 -4.5)
          w1 (m/complex -6.7 8.9)
          w2 (m/complex 1.3 -5.7)
          w3 (m/complex -2.4 6.8)
          mappings [[z1 w1] [z2 w2] [z3 w3]]
          A (apply m/mobius-by-3-pts mappings)]
      (dorun (for [[z w] mappings]
               (is (approx= (->> z
                                 (m/affine->proj)
                                 (m/proj-matvecmul-2x2 A)
                                 (m/proj->affine))
                            w 1e-4)
                   "Returned Möbius transformation satisfies the
                   mapping constraints")))))
  
  (testing "Möbius transformation that preserves the disk boundary and
           preserves orientation should belong to PSU(1, 1)"
    (let [z1 (m/cis 0)
          z2 (m/cis 0.2)
          z3 (m/cis 0.4)
          w1 (m/cis 0.1)
          w2 (m/cis 0.3)
          w3 (m/cis -0.5)
          ; This map should be orientation-preserving, as the cyclical order of
          ; the w's (3 -> 1 -> 2) is the same as the cyclical order of the z's
          ; (1 -> 2 -> 3, which is cyclically equivalent to 3 -> 1 -> 2).
          mappings [[z1 w1] [z2 w2] [z3 w3]]
          [u1 v1
           v2 u2 :as A] (apply m/mobius-by-3-pts mappings)]
      (let [z4 (m/cis 0.6)
            w4 (->> z4
                    (m/affine->proj)
                    (m/proj-matvecmul-2x2 A)
                    (m/proj->affine))]
        (is (approx= (abs w4) 1 1e-4)
            "If three points on the unit circle stay on the unit circle, then
            all points on the unit circle stay on the unit circle"))

      ; Check directly against the form [u v; v* u*] of a generic element of
      ; SU(1, 1).  To do this, first divide through by the scalar s so that the
      ; u's become conjugates of each other, then check that the v's also
      ; become conjugates of each other.
      (let [s (sqrt (* u1 u2))
            [uu1 vv1
             vv2 uu2 :as AA] (map #(/ % s) A)]
        (is (approx= uu1 (m/complex-conj uu2) 1e-4))
        (is (approx= vv1 (m/complex-conj vv2) 1e-4)
            "Möbius transformation that preserves the unit circle and preserves
            orientation can be represented in the form [u v; v* u*]")))))

(deftest apply-mobius-to-seg
  (testing "apply-mobius-to-seg"
    (let [old-ctr [1.45 -0.6]
          old-seg (-> (h-line old-ctr)
                      (assoc ::c/start-angle 2.9
                             ::c/delta-angle 0.1)
                      (#(assoc % ::c/h-length (h-length-of %))))
          [old-bwd old-fwd] (h-line-end-pts-bwd-fwd old-seg)
          bearing 0.123
          xform (g/shift-to-1p old-seg bearing)]
      (is (approx= (normsq old-bwd)
                   1 1e-4))
      (is (approx= (normsq old-fwd)
                   1 1e-4))
      (let [{new-ctr ::c/center
             :as new-seg}  (m/apply-mobius-to-seg xform old-seg)
            [new-bwd new-fwd] (h-line-end-pts-bwd-fwd new-seg)]
        (is (approx= (h-length-of new-seg)
                     (h-length-of old-seg) 1e-4)
            "Applying isometry to seg preserves hyperbolic length")
        (is (approx= (normsq new-bwd) 1 1e-4)
            "bwd endpoint of transformed seg is on the boundary of the disk")
        (is (approx= (normsq new-fwd) 1 1e-4)
            "fwd endpoint of transformed seg is on the boundary of the disk")
        (is (approx-perp-2d? (-v new-ctr new-bwd) new-bwd 1e-4)
            "Transformed arc is perpendicular to the disk boundary at bwd")
        (is (approx-perp-2d? (-v new-ctr new-fwd) new-fwd 1e-4)
            "Transformed arc is perpendicular to the disk boundary at fwd")
        (is (all-approx= (m/apply-mobius-R2 xform (arc-start-pt old-seg))
                         (arc-start-pt new-seg) 1e-4)
            "Start point is preserved")
        (is (all-approx= (m/apply-mobius-R2 xform (arc-end-pt old-seg))
                         (arc-end-pt new-seg) 1e-4)
            "Start point is preserved")
        (is (< (::c/line-angle-min new-seg)
               (::c/start-angle new-seg)
               (::c/line-angle-max new-seg))
            "start-angle is within bounds")
        (is (< (::c/line-angle-min new-seg)
               (+ (::c/start-angle new-seg)
                  (::c/delta-angle new-seg))
               (::c/line-angle-max new-seg))
            "start-angle + delta-angle is within bounds")))))


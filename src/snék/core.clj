(ns snék.core
  "Logic and specs for the geometric primitives and geometric operations on
  them."
  (:require [clojure.spec.alpha :as s]
            [clojure.algo.generic.math-functions :refer (approx=)]
            [snék.core-util :refer :all]))

;; ==== Computing and drawing hyperbolic line segments ====

(s/def ::point2d
  ;; Point in the plane, described by its Euclidean coordinates [x y].
  (s/coll-of number? :count 2))

(s/def ::point-inside-disk
  ;; Point in the plane (`::point2d`) that lies inside or on the boundary of
  ;; the Poincare disk (i.e., the unit disk).
  (s/and ::point2d
         #(<= (normsq %1) 1)))

(s/def ::point-outside-disk
  ;; Point in the plane that lies strictly outside the Poincare disk (i.e., the
  ;; unit disk).
  (s/and ::point2d
         #(> (normsq %1) 1)))

(s/def ::point-on-disk-boundary
  ;; Point on the boundary of the Poincare disk (i.e., the unit circle).
  ;; From the hyperbolic standpoint, these are points at infinity, aka
  ;; "ideal" points.
  (s/and ::point2d
         #(approx= (normsq %1) 1 1e-4)))

(s/def ::vec2d
  ;; Vector in the plane, described by its Euclidean coordinates [x y].
  ;; There is not always a clear distinction between points (`::point2d`) and
  ;; vectors, but when applicable, a "vector" is conceptually an element of the
  ;; tangent bundle, that is, the space of "velocities" at a particular point
  ;; in space.
  (s/coll-of number? :count 2))

(s/def ::center
  ;; Euclidean center of a hyperbolic line (= Euclidean arc whose extension is
  ;; perpendicular to the disk).  The infinite-radius case (diameters of the
  ;; disk) is currently not supported.
  ::point-outside-disk)

(s/def ::radius
  ;; Euclidean radius of a hyperbolic line (= Euclidean arc whose extension is
  ;; perpendicular to the disk).  The infinite-radius case (diameters of the
  ;; disk) is currently not supported.
  number?)

(s/def ::absolute-angle
  ;; "Absolute" central angle (in radians) identifying a point on a hyperbolic
  ;; line (= Euclidean arc whose extension is perpendicular to the disk).  Not
  ;; guaranteed to be between 0 and 2pi.  An angle of 0 (or 2pi, or -6pi, etc.)
  ;; corresponds to the point on the circle with the largest x-value.
  number?)

(s/def ::start-angle
  ;; Absolute angle (see `::absolute-angle`) of the starting point of a
  ;; directed hyperbolic line segment.
  ::absolute-angle)

(s/def ::delta-angle
  ;; Euclidean central angle (in radians) of a directed hyperbolic line segment
  ;; (= directed Euclidean arc).  Must be nonzero, i.e., degenerate arcs are
  ;; not allowed.  Positive if the direction of the arc is positive (for
  ;; right-handed coordinate systems this means counter-clockwise), negative
  ;; otherwise.  This central angle is "relative" in the sense that it is the
  ;; signed difference between the two "absolute" angles (see
  ;; `::absolute-angle`) of its endpoints, for appropriate choice of additive
  ;; multiple of 2pi.
  number?)

(s/def ::h-length
  ;; Length of a hyperbolic line segment.  For the definition of distance in
  ;; the Poincare disk, see e.g.
  ;; https://en.wikipedia.org/wiki/Poincar%C3%A9_disk_model#Distance
  (s/or :pos pos? :zero zero?))

(s/def ::line-angle-min
  ;; "Absolute" central angle (in radians) of the lower endpoint of a
  ;; hyperbolic line (= Euclidean arc that is perpendicular to the disk and
  ;; whose endpoints are on the boundary of the disk).  Not guaranteed to be
  ;; between 0 and 2pi.  However, if `::line-angle-min` and `::line-angle-max`
  ;; are both present, it is required that
  ;;
  ;;     line-angle-min < line-angle-max <= line-angle-min + pi.
  ;;
  ;; For example, (line-angle-min, line-angle-max) could be (-0.2, -0.1) but it
  ;; couldn't be (-0.2, -0.1 + 2pi).
  ;;
  ;; An angle of 0 (or 2pi, or -6pi, etc.) corresponds to the point on the
  ;; circle with the largest x-value.
  ::absolute-angle)

(s/def ::line-angle-max
  ;; Counterpart of `::line-angle-min` for the upper endpoint of the hyperbolic
  ;; line.  See doc for `::line-angle-min`.
  ::absolute-angle)

(s/def ::angle-dir
  ;; There are several ways to interpret this concept.  Probably the simplest
  ;; way is as a direction of rotation.  In a right-handed coordinate system,
  ;; the positive direction (`:pos`) is counter-clockwise and the negative
  ;; direction (`:neg`) is clockwise.  In a left-handed coordinate system, the
  ;; directions are swapped.
  ;;
  ;; A second way to interpret this concept is as a direction of change in
  ;; absolute angle (see `::absolute-angle): `:pos` means increasing and `:neg`
  ;; means decreasing.
  ;;
  ;; A third interpretation, very similar to the second one, is that an
  ;; `angle-dir` is the direction of a directed arc.  This is relevant in a
  ;; snake game because the player determines the arc directions through their
  ;; key-presses that turn the snake's head (though the mapping from key press
  ;; to `angle-dir` depends on where in the disk the snake's head is).
  #{:pos :neg})

(s/def ::h-line
  ;; Hyperbolic line segment in the Poincare disk, including pre-computed
  ;; geometry for its Euclidean representation as an arc.   The infinite-radius
  ;; case (diameters of the disk) is currently not supported.
  (s/keys :req [::center
                ::radius
                ::line-angle-min
                ::line-angle-max]))

(s/def ::seg
  ;; Directed hyperbolic line segment, including pre-computed geometry for its
  ;; Euclidean representation as an arc and pre-computed hyperbolic length
  ;; (`::h-length`).  There is no explicit `::angle-dir` in this spec; instead,
  ;; the direction of the arc is considered positive if `delta-angle > 0` and
  ;; negative if `delta-angle < 0`.
  (s/and
    ::h-line
    (s/keys :req [::start-angle ::delta-angle ::h-length])))

(s/def ::arc-without-h-line
  ;; Euclidean arc.  Does not necessarily have "hyperbolic line segment"
  ;; semantics.
  (s/keys :req [::center ::radius ::start-angle ::delta-angle]))

(s/fdef point-on-circle :args (s/cat :center ::point2d
                                     :radius ::non-neg
                                     :angle ::absolute-angle))
(defn point-on-circle
  "Returns the point (`::point2d`) on the circle with the given center, radius,
  and absolute angle (`::absolute-angle`)."
  [center radius angle]
  (+v center
      (scalev radius (cossin angle))))

(s/fdef h-line-end-pts-bwd-fwd
        :ret (s/coll-of ::point-on-disk-boundary :count 2))
(defn h-line-end-pts-bwd-fwd
  "Returns the endpoints of the hyperbolic line containing `seg`, in a specific
  order: first the 'backward' endpoint, then the 'forward' endpoint.

  Here backwards and forwards are determined by the sign of `delta-angle`: if
  `delta-angle` is positive (resp. negative), then `line-angle-max` (resp.
  `line-angle-min`) is the forward endpoint."
  [{::keys [center radius
            line-angle-min line-angle-max
            start-angle delta-angle]
    :as seg}]
  (let [angles (cond-> [line-angle-min
                        line-angle-max]
                 (neg? delta-angle) reverse)]
    (map #(point-on-circle center radius %)
         angles)))

(s/fdef circum-center :args (s/cat :x ::point2d
                                   :y ::point2d
                                   :z ::point2d)
        :ret ::point2d)
(defn circum-center
  "Returns the center of the circumscribed (Euclidean) circle of the triangle
  with vertices `x`, `y` and `z`.  It is assumed that `x`, `y` and `z` are not
  (Euclidean) collinear."
  [[x1 y1 :as pt1]
   [x2 y2 :as pt2]
   [x3 y3 :as pt3]]
  (let [m1-2 (halfv (+v pt1 pt2))
        m1-3 (halfv (+v pt1 pt3))
        ; The perpendicular bisector of the side pt1--pt2 is
        ;
        ;     m1-2 + c * [y1 - y2,
        ;                 x2 - x1]   | c in R.
        ;
        ; The perpendicular bisector of the side pt1--pt3 is
        ;
        ;     m1-3 + d * [y1 - y3,
        ;                 x3 - x1]   | d in R.
        ;
        ; Solving for the value of [c, d] corresponding to the intersection of
        ; the two perpendicular bisectors:
        ;
        ;     m1-2 + c * [y1 - y2,
        ;                 x2 - x1] = m1-3 + d * [y1 - y3,
        ;                                        x3 - x1]
        ;
        ;     c * [y1 - y2,
        ;          x2 - x1] - d * [y1 - y3,
        ;                          x3 - x1] = m1-3 - m1-2
        ;
        ;     M @ [c, -d] = m1-3 - m1-2   where M := [y1 - y2,   y1 - y3;
        ;                                             x2 - x1,   x3 - x1]
        ;
        ;     [c, -d] = M \ (m1-3 - m1-2)
        ;
        ; Then the point we're looking for is
        ;
        ;     m1-2 + c * [y1 - y2,
        ;                 x2 - x1].
        ;
        [c _] (matvecdiv-2x2 [(- y1 y2)  (- y1 y3)
                              (- x2 x1)  (- x3 x1)]
                             (-v m1-3 m1-2))]
    (+v m1-2 (scalev c [(- y1 y2)
                        (- x2 x1)]))))

(s/fdef arc-start-pt :args (s/cat :arc ::arc-without-h-line))
(defn arc-start-pt
  "Returns the 'start' point of `arc`, that is, the point with absolute
  angle `start-angle`."
  [{::keys [center radius start-angle] :as arc}]
  (point-on-circle center radius start-angle))

(s/fdef arc-end-pt :args (s/cat :arc ::arc-without-h-line))
(defn arc-end-pt
  "Returns the 'end' point of `arc`, that is, the point with absolute angle
  `start-angle + delta-angle`."
  [{::keys [center radius start-angle delta-angle] :as arc}]
  (point-on-circle center radius (+ start-angle delta-angle)))

(defn opposite-angle-dir [angle-dir]
  ({:pos :neg
    :neg :pos} angle-dir))

(defn mul-angle-dirs
  "'Multiplies' the given `angle-dir`s.  This can be thought of as actual
  multiplication of ±1, or as composition of maps between curves that are either
  orientation-preserving or orientation-reversing."
  [& angle-dirs]
  (if (even? (count (filter #{:neg} angle-dirs)))
    :pos
    :neg))

(defn which-way-from-midpt
  "Returns the direction (`::angle-dir`) from the 'midpoint' of the hyperbolic
  line with Euclidean center `center` to the point `pt`.  The point `pt` is
  assumed to lie on the hyperbolic line.

  Here the notion of 'midpoint' of a hyperbolic line is simply the midpoint of
  the Euclidean arc representing the hyperbolic line; this is not an intrinsic
  hyperbolic geometric notion but rather is specific to the Poincaré disk."
  [pt center]
  (let [pt-rdir (-v pt center)
        arc-mid-rdir (-v center)]
    (if (pos? (scalar-cross pt-rdir arc-mid-rdir))
      :pos
      :neg)))

(defn h-line
  "Computes the radius and the minimum and maximum absolute angle of the
  hyperbolic line with the given Euclidean center.  See spec doc of
  `::line-angle-min` for more details, including the order contract of
  `::line-angle-min` and `::line-angle-max`."
  [center]
  (let [angle-mid (angle-of (-v center))
        d (apply hypot center)
        radius (Math/sqrt (- (* d d) 1))
        angle-half-width (Math/asin (/ 1 d))
        [angle-low angle-high] (∓ angle-mid angle-half-width)]
    {::center center
     ::radius radius
     ::line-angle-min angle-low
     ::line-angle-max angle-high}))

(s/fdef with-h-line
        :args (s/cat :partial-line (s/keys :req [::center]
                                           :optional [::line-angle-min
                                                      ::line-angle-max
                                                      ::radius]))
        :ret ::h-line)
(defn with-h-line
  "If keys `::line-angle-min`, `::line-angle-max` and `::radius` are present,
  does nothing.  Otherwise, computes their values (see spec docs for those
  keys) and assocs them to `partial-line`."
  [partial-line]
  (if (contains-all? partial-line [::line-angle-min ::line-angle-max ::radius])
    partial-line
    (merge partial-line (h-line (::center partial-line)))))

(defn h-length-of
  "Returns the hyperbolic length of `partial-seg` (which would be a `::seg`
  except that its `::h-length` is not yet precomputed)."
  [{::keys [center radius
            line-angle-min line-angle-max
            start-angle delta-angle]
    :as partial-seg}]
  (let [[A B P Q] (map #(point-on-circle center radius %)
                       [line-angle-min
                        line-angle-max
                        start-angle
                        (+ start-angle delta-angle)])]
    (Math/abs (Math/log (/ (* (e-dist A P) (e-dist B Q))
                           (* (e-dist A Q) (e-dist B P)))))))

(defn rerep-and-order-line-angle-extremes
  "Replaces `angle1` and `angle2` with (possibly) new representatives mod 2π,
  and reorders them, so that they can be used as a `::line-angle-min` and a
  `::line-angle-max`.  There is a unique way to do this, up adding a multiple
  (the same multiple) of 2π to both values.

  More concretely: returns a pair `[new-angle1, new-angle2]`, where

      new-angle1 <= new-angle2 < new-angle1 + π

  and either

      new-angle1 ≡ angle1 and new-angle2 ≡ angle2 mod 2π

  or

      new-angle1 ≡ angle2 and new-angle2 ≡ angle1 mod 2π.
  "
  [angle1 angle2]
  (let [closer-angle2 (residue-just-above angle2 angle1 _2pi)]
    (if (<= (- closer-angle2 angle1) Math/PI)
      [angle1 closer-angle2]
      (let [closer-angle1 (residue-just-above angle1 angle2 _2pi)]
        (assert (< (- closer-angle1 angle2) (+ Math/PI 1e-10))
                "Bug: neither order works")
        [angle2 closer-angle1]))))

(s/fdef seg-from-start-length
        :args (s/cat :partial-seg (s/keys :req [::center
                                                ::start-angle
                                                ::h-length]
                                          :opt [::radius
                                                ::line-angle-min
                                                ::line-angle-max])
                     :rest (s/keys* :req [::angle-dir]))
        :ret ::seg)
(defn seg-from-start-length
  "Computes the hyperbolic line segment (see spec doc for `::seg`) of the
  directed hyperbolic line segment with the given center, start angle,
  hyperbolic length, and direction (`::angle-dir`).

  The formulas used in this function ar derived in `docs/geodesic.pdf`.

  If the keys `::radius`, `::line-angle-min` and `::line-angle-max` are all
  provided, they will not be recomputed."
  [partial-seg & {::keys [angle-dir]}]
  (let [{::keys [center radius line-angle-min line-angle-max
                 start-angle h-length]
        :as seg-v1}  (with-h-line partial-seg)
        pt-on-line (fn [angle] (point-on-circle center radius angle))
        [angle-A angle-B] (map seg-v1
                               (-> [::line-angle-min ::line-angle-max]
                                   ((angle-dir {:pos identity :neg reverse}))))
        sign (angle-dir {:pos 1 :neg -1})
        α (* 0.5 (- start-angle angle-A))
        ω (* 0.5 (- angle-B angle-A))
        b (* (Math/exp h-length)
             (/ (Math/sin α)
                (Math/sin (- ω α))))
        η (-> (Math/atan2
                (* sign b)
                (+ (* b
                      (Math/sqrt (- (normsq center) 1)))
                   (norm center)))
              (residue-just-above (* -0.5 Math/PI)
                                  Math/PI))
        γ (- η α)]
    (assoc seg-v1 ::delta-angle (* 2 γ))))

(s/fdef center-from-point-r-dir
        :args (s/cat :point ::point-inside-disk
                     :r-dir (s/and ::vec2d #(not-every? zero? %1)))
        :ret ::center)
(defn center-from-point-r-dir
  "Returns the Euclidean center of the hyperbolic line (= Euclidean arc that
  is perpendicular to the disk and whose endpoints are on the disk boundary)
  that satisfies the following two conditions:
  (1) The arc contains `point`.
  (2) The Euclidean vector from the center of the arc to `point` is a scalar
      multiple of `r-dir`."
  [point r-dir]
  (let [r (/ (- (normsq point) 1)
             (* 2 (dot point r-dir)))
        r-vec (scalev r r-dir)]
    (-v point r-vec)))

(s/fdef perpendicular-center
        :args (s/cat :center ::center
                     :pt ::point-inside-disk))
(defn perpendicular-center
  "Returns the Euclidean center of the hyperbolic perpendicular at point `pt`
  to the hyperbolic line with center `center`.  Requires the point `pt` to
  actually lie on that line, or else the result is unspecified."
  [center pt]
  (let [r-dir (quarter-turns (-v pt center)
                             1)]
    (center-from-point-r-dir
      pt r-dir)))

(defn turn-from-seg
  "Computes the hyperbolic line segment that results from turning the
  snake's 'head' a quarter turn in direction `turn-dir` and proceeding
  forward by `h-length `hyperbolic length units.  `h-length `must be positive,
  so that the resulting segment has a well-defined direction.

  TODO: Test this function"
  [{prev-center ::center
    prev-radius ::radius
    prev-start-angle ::start-angle
    prev-delta-angle ::delta-angle
    :as prev-seg}
   h-length
   turn-dir]
  (let [prev-end-angle (+ prev-start-angle prev-delta-angle)
        prev-endpt  (point-on-circle prev-center prev-radius prev-end-angle)
        prev-angle-dir (if (>= prev-delta-angle 0)
                         :pos
                         :neg)
        center      (perpendicular-center prev-center
                                          prev-endpt)
        start-pt    prev-endpt
        {::keys [line-angle-min
                 line-angle-max]
         :as line}  (h-line center)
        start-angle (-> (angle-of (-v start-pt center))
                        ; If `start-pt` is far away from the arc midpoint of
                        ; `line`, it can happen that we need to add a multiple
                        ; of 2pi to the preceding value to make it be between
                        ; `line-angle-min` and `line-angle-max`.  To handle
                        ; that:
                        (residue-just-above line-angle-min
                                            _2pi))
        angle-dir (mul-angle-dirs (which-way-from-midpt start-pt
                                                        prev-center)
                                  prev-angle-dir
                                  turn-dir)
        seg (seg-from-start-length
              (merge line {::start-angle start-angle
                           ::h-length h-length})
              ::angle-dir angle-dir)]
    (assert (<= line-angle-min start-angle line-angle-max)
            "Internal error: computed angle is out of range")
    seg))

(defn change-length-moving-start-pt
  "Changes the hyperbolic length of `seg` to be `new-h-length`, preserving
  the *end point* and arc direction.  The fields changed are `::delta-angle`,
  `::start-angle`, and `::h-length`.

  TODO: Test this function"
  [seg new-h-length]
  (let [angle-dir (if (pos? (::delta-angle seg)) :pos :neg)]
    (as-> seg $
      (update $ ::start-angle + (::delta-angle $))
      (dissoc $ ::delta-angle)
      (assoc $ ::h-length new-h-length)
      (seg-from-start-length $ ::angle-dir (opposite-angle-dir
                                             angle-dir))
      (update $ ::start-angle + (::delta-angle $))
      (update $ ::delta-angle -))))

(s/fdef arc-intersections
        :args (s/cat :arc1 ::arc-without-h-line
                     :arc2 ::arc-without-h-line)
        :ret (s/coll-of ::absolute-angle :max-length 2))
(defn arc-intersections
  "Returns the absolute angles, with respect to the circle containing `arc1`,
  of all intersection points of `arc1` and `arc2`.  There are at most two
  intersection points."
  [arc1 arc2]
  (if-not (satisfies-triangle-inequality? (::radius arc1)
                                          (::radius arc2)
                                          (norm (-v (::center arc1)
                                                    (::center arc2))))
    []
    (let [{ctr1 ::center r1 ::radius} arc1
          {ctr2 ::center r2 ::radius} arc2
          [ctr-dist θ] (to-polar (-v ctr2 ctr1))
          α (triangle-angle-from-side-lengths
              r1 ctr-dist r2)
          intersection-angles-rel-arc1 (∓ θ α)

          β (triangle-angle-from-side-lengths
              r2 ctr-dist r1)
          ;; Note: ±, whereas `intersection-angles-rel-arc1` was ∓.  Order is
          ;; reversed, essentially because the math for arc2 works out in
          ;; mirror image, i.e. with arc2 oriented the opposite way to arc1.
          intersection-angles-rel-arc2 (± (+ θ Math/PI)
                                          β)

          in-arc? (fn [angle {::keys [start-angle delta-angle] :as arc}]
                    (let [[angle-low angle-high]
                          (min-max start-angle
                                   (+ start-angle delta-angle))]
                      (in-range-mod? angle
                                     angle-low
                                     angle-high
                                     _2pi)))]

      (for [[angle-rel-arc1
             angle-rel-arc2] (zip intersection-angles-rel-arc1
                                  intersection-angles-rel-arc2)
            :when (and (in-arc? angle-rel-arc1 arc1)
                       (in-arc? angle-rel-arc2 arc2))]
        angle-rel-arc1))))

(defn seg-end-bearing
  "Returns the 'bearing' of `arc` at its endpoint, that is, the polar angle of
  the tangent vector to `arc` at its endpoint.

  If we think of the endpoint of `arc` as being the snake's head, then this
  angle corresponds to the direction the snake is facing."
  [{::keys [start-angle delta-angle] :as arc}]
  (let [end-angle (+ start-angle delta-angle)]
    (+ end-angle (if (pos? delta-angle)
                   _halfpi
                   (- _halfpi)))))

(s/fdef h->e-circle :args (s/cat :h-center ::point-inside-disk
                                 :h-radius pos?))
(defn h->e-circle
  "Returns `[e-center e-radius]`, the Euclidean center and Euclidean radius of
  the circle with hyperbolic center `h-center` and hyperbolic radius
  `h-radius`."
  [h-center h-radius]
  ;; Math derivation: let C := `h-center`, let AB be the diameter of the disk
  ;; containing C, and let P and Q be the (yet unknown) intersections of AB
  ;; with the hyperbolic circle.  Then PQ is a diameter of the circle in both
  ;; the hyperbolic sense and the Euclidean sense.  Without loss of generality,
  ;; assume A, P, Q, B appear in that order along the line AB.  Let
  ;; r := |AP| - 1 and s := |AQ| - 1.  Let D := exp(`h-radius`).  Then we have
  ;;
  ;;     (AQ * BC) / (AC * BQ) = D
  ;;
  ;; which is
  ;;
  ;;     (1 + s) / (1 - s) = (AC/BC) * D.
  ;;
  ;; This gives
  ;;
  ;;     s = 1 - 2 / ((AC/BC) * D + 1).
  ;;
  ;; Similarly, we have
  ;;
  ;;     (AC * BP) / (AP * BC) = D
  ;;
  ;; which is
  ;;
  ;;     (1 - r) / (1 + r) = (BC/AC) * D
  ;;
  ;; and this gives
  ;;
  ;;     r = 2 / ((BC/AC) * D + 1) - 1.
  ;;
  ;; Also, we have BC = 1 - norm(`h-center`) and AC = 1 + norm(`h-center`).
  ;; This information tells us what P and Q are.  The Euclidean center of the
  ;; circle is then the midpoint of PQ, and the Euclidean radius is PQ/2 =
  ;; (s - r) / 2.
  (let [t (norm h-center)
        D (Math/exp h-radius)
        AC (+ 1 t)
        BC (- 1 t)
        r (- (/ 2
                (+ 1
                   (* (/ BC AC)
                      D)))
             1)
        s (- 1
             (/ 2
                (+ 1
                   (* (/ AC BC)
                      D))))
        e-radius (* 1/2 (- s r))
        e-center (if (zero? (normsq h-center))
                   [0 0]
                   (scalev (* 1/2 (+ s r))
                           (unit h-center)))]
    [e-center e-radius]))

(ns snék.draw
  "Logic for drawing game elements in the JavaFX GUI."
  (:require [clojure.spec.alpha :as s]
            [snék.core-util :refer :all]
            [snék.core :as c :refer :all]
            [snék.config :as config]
            [snék.extra-spec :as es])
  (:import (javafx.scene Scene Node)
           (javafx.scene.paint Color)
           (javafx.scene.shape Ellipse Arc Circle Polygon Rectangle StrokeLineCap)))

;; ==== Conversion from world coordinates to JavaFX coordinates ====
;;
;; "World coordinates" here are a frame of my choosing.  Geometric calculations
;; generally happen in world coordinates, with conversion to JavaFX coordinates
;; happening only right before rendering.  I have generally found this to be
;; the cleanest approach to working with an external renderer while writing
;; geometric code, as the coordinate frames of renderer APIs are often hard to
;; get right.  So I'd rather have easy/correct geometry in a frame I understand
;; + possibly-incorrect conversion to renderer coordinates, rather than leaking
;; confusion into the whole codebase by trying to do everything in the
;; renderer's frame.  (Yes, I realize the irony of this since everyone thinks
;; their coordinate system is the easiest.)
;;
;; In world coordinates, +x points right and +y points up.
;;
;; In JavaFX coordinates, +x points right, +y points down, and lengths are
;; measured in pixels (and are not necessarily integers).  Thus, the conversion
;; from world coordinates to JavaFX coordinates depends on what region of the
;; world is being displayed in what region of the JavaFX window.  This
;; information is captured by a `::config/jfx-region`, and we call them
;; "region coordinates."  For example, if we are displaying the square in world
;; coordinates `0 <= x <= 1, 0 <= y <= 1` within the square `100 <= x <= 200,
;; 300 <= y <= 400`, then the conversion from world coordinates to region
;; coordinates is `(x, y) -> (100 + 100*x, 400 - 100*y)`.

(s/fdef jfx-scale :args (s/cat :jfx-region ::config/jfx-region))
(defn jfx-scale
  "Returns a vector `[scale-x scale-y]` containing the factor by which lengths
  expand in each direction when passing from world coordinates to region
  coordinates.  In other words, a length of 1 parallel to the x-axis
  (respectively, y-axis) in world coordinates is equivalent to a length of
  `scale-x` (respectively, `scale-y`) in region coordinates.

  Note that `scale-x` and `scale-y` are positive, even though the y-axes in
  world and JavaFX coordinates are oppositely oriented."
  [jfx-region]
  (mapv / (::config/region-size-px jfx-region)
        (::config/region-size jfx-region)))

(s/fdef world->jfx :args (s/cat :pt ::c/point2d
                                :jfx-region ::config/jfx-region))
(defn world->jfx [pt jfx-region]
  "Converts `pt` from world coordinates to region coordinates."
  (->> (-v pt (::config/region-center
                jfx-region))   ; translate so that the `::config/region-center`
                               ; becomes (0, 0)
       (mapv *
             [1 -1]            ; flip the y-axis
             (jfx-scale jfx-region))  ; scale each axis from world scale to jfx
                                      ; pixel scale
       (+v (::config/region-center-px
             jfx-region))))    ; translate so that (0, 0) becomes the
                               ; `::config/region-center-px`

(s/fdef make-disk-node :args (s/cat :jfx-region ::config/jfx-region))
(defn make-disk-node
  "Create a JavaFX node for the boundary of the Poincare disk.
  The disk is centered at (0, 0) and has radius 1, both in world coordinates.
  The JavaFX rendering places the disk center at the center of `jfx-region`."
  [jfx-region]
  (let [center-jfx (world->jfx [0 0] jfx-region)
        scale (jfx-scale jfx-region)]
    (doto (Ellipse. (center-jfx 0)
                    (center-jfx 1)
                    (scale 0)
                    (scale 1))
      (.setFill nil)
      (.setStroke Color/GRAY)
      (.. (getStrokeDashArray) (setAll [4. 3.]))
      (.setStrokeDashOffset 2.))))

(defn set-fence-shape!
  "Mutates `fence-node` so that its center (in world coordinates) is `center`
  and its Euclidean radius (in world coordinates) is `radius`."
  [fence-node center radius jfx-region]
  (let [center-jfx (world->jfx center jfx-region)
        radii-jfx (scalev radius (jfx-scale jfx-region))]
    (doto fence-node
      (.setCenterX (center-jfx 0))
      (.setCenterY (center-jfx 1))
      (.setRadiusX (radii-jfx 0))
      (.setRadiusY (radii-jfx 1)))))

(s/fdef make-fence-node :args (s/cat :jfx-region ::config/jfx-region
                                     :e-radius #(< 0 % 1)))
(defn make-fence-node
  "Create a JavaFX node for the fence.
  The fence is centered at (0, 0) and has Euclidean radius
  `::config/fence-radius`, both in world coordinates."
  [jfx-region e-radius]
  (doto (Ellipse.)
    (set-fence-shape! [0 0]
                      e-radius
                      jfx-region)
    (.setFill nil)
    (.setStroke Color/SADDLEBROWN)
    (.setStrokeWidth 2.)))

(defn make-seg-node
  "Create a JavaFX node for the given hyperbolic line segment in the Poincare
  disk.  A hyperbolic line segment is a Euclidean arc whose extension meets
  the disk boundary at a right angle.  The infinite-radius case (exact
  diameters of the disk) is currently not supported."
  [{::c/keys [center radius start-angle delta-angle]}
   jfx-region]
  (let [center-jfx (world->jfx center jfx-region)
        radii-jfx (scalev radius
                          (jfx-scale jfx-region))
        angle-low (min start-angle
                       (+ start-angle delta-angle))]
    (-> (Arc. (center-jfx 0)
              (center-jfx 1)
              (radii-jfx 0)
              (radii-jfx 1)
              (rad->deg angle-low)
              (rad->deg (Math/abs delta-angle)))
        (doto
          (.setFill nil)
          (.setStroke Color/GREEN)
          ;; No decoration on the stroke line cap, because we don't want
          ;; the arc to protrude past the tip of the arrowhead.
          (.setStrokeLineCap StrokeLineCap/BUTT)))))

(defn set-jfx-arc-angles-from-seg!
  "Sets the `startAngle` and `length` fields of `jfx-node` to correspond to
  the geometry of `seg`.  (Design note: Future versions of this function may
  need to also set the radius fields, but that's not currently necessary.)"
  [^Arc jfx-node {::c/keys [start-angle delta-angle]
                  :as seg}]
  (let [angle-low (min start-angle
                       (+ start-angle delta-angle))]
    (.setStartAngle jfx-node (rad->deg angle-low))
    (.setLength jfx-node (rad->deg (Math/abs delta-angle)))))

(s/fdef make-apple-point-node
        :args (s/cat :apple-location ::es/point-inside-fence
                     :jfx-region ::config/jfx-region))
(defn make-apple-point-node
  "Create a JavaFX node for a dot at the location of the apple."
  [apple-location jfx-region]
  (let [[x y] (world->jfx apple-location jfx-region)]
    (Circle. x y 3. Color/CRIMSON)))

(s/fdef make-apple-collision-region-node
        :args (s/cat :e-center ::es/point-inside-fence
                     :e-radius pos?
                     :jfx-region ::config/jfx-region))
(defn make-apple-collision-region-node
  "Create a JavaFX node for an outline of the collision region of the apple."
  [e-center e-radius jfx-region]
  (let [[x y] (world->jfx e-center jfx-region)
        [r-x r-y] (scalev e-radius
                          (jfx-scale jfx-region))]
    (doto (Ellipse. x y r-x r-y)
      (.setFill nil)
      (.setStroke Color/LIGHTGRAY)
      (.setStrokeWidth 1.))))

(defn make-arrowhead-node
  "Create a JavaFX node for the arrowhead.  The returned `Polygon` has the
  right fill color but no spatial extent until a mutator like
  `move-arrow-head-to-arc-end-pt!` is called on it to set the location and
  orientation."
  []
  (doto (Polygon.)
    (.setFill Color/DARKGREEN)))

(s/fdef move-arrowhead-to-arc-end-pt! :args (s/cat :arrowhead any?
                                                   :arc ::c/arc-without-h-line
                                                   :jfx-region ::config/jfx-region))
(defn move-arrowhead-to-arc-end-pt!
  "Mutates `arrowhead` (a JavaFX `Polygon` node in the shape of a triangle) so
  that it points in the direction of `arc`."
  [arrowhead
   {::c/keys [start-angle delta-angle] :as arc}
   jfx-region]
  (let [end-angle (+ start-angle delta-angle)
        turn-angle (* Math/PI
                      (if (pos? delta-angle)
                        1/2 -1/2))
        apex-angle (+ end-angle turn-angle)
        [x y :as apex-pt-jfx] (world->jfx
                                (arc-end-pt arc)
                                jfx-region)
        base-pts-jfx (map #(->> (cossin %1)
                                ;; The arrowhead is a visual aid, not an object
                                ;; in the world.  So we specify its size in
                                ;; pixels, not world coordinates.
                                (scalev 5)
                                ;; The y-axis points down in JavaFX
                                ;; coordinates, whereas it points up in world
                                ;; coordinates.  So flip the y-axis, same as we
                                ;; do in `world->jfx`.
                                (map * [1 -1])
                                (+v apex-pt-jfx))
                          (± apex-angle
                             (* Math/PI 5/6)))]
    (.. arrowhead (getPoints)
        (setAll (apply concat apex-pt-jfx base-pts-jfx)))))

(defn make-box-around
  "Returns a JavaFX `Rectangle` outlining the given region."
  [{::config/keys [region-center-px region-size-px]}]
  (let [[pos-x pos-y] (-v region-center-px
                          (halfv region-size-px))
        [width height] region-size-px]
    (doto (Rectangle. pos-x pos-y width height)
      (.setStroke Color/BLACK)
      (.setFill nil))))

(ns snék.main
  "Entry point for the game, including command line argument parsing."
  (:require [snék.game]
            [snék.config]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :refer (pprint)]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [clojure.tools.cli :as cli])
  (:import (java.io PushbackReader)
           (javafx.application Application))
  (:gen-class))

(def cli-options
  [[nil "--config-file PATH"
    "Path to user-specified config file"]
   ["-h" "--help" "Print brief help text and exit"]])

(defn usage [options-summary]
  (->> ["Snake game in the Poincaré disk."
        ""
        "Options:"
        options-summary]
       (string/join "\n")))

(defn validate-first-problem
  "Returns nil if `x` conforms to `spec`; otherwise, returns the first problem
  reported by `clojure.spec.alpha/explain-data`."
  [x spec]
  (some-> (s/explain-data spec x)
          (::s/problems)
          (first)))

(defn load-and-validate-config
  "Loads and validates a user-supplied config from (the first EDN object in)
  the file located at `path`.  If `path` is nil, then acts as if the
  user-supplied config is empty.  Returns the full config map (gotten by
  merging the user-defined config with the default config) if validation
  passes, otherwise throws an `IllegalArgumentException`."
  [path]
  (let [u-config (if path
                   (with-open [r (io/reader path)]
                     (edn/read (PushbackReader. r)))
                   {})]
    (if-some [err (validate-first-problem u-config :snék.config/user-config)]
      (throw (java.lang.IllegalArgumentException.
               (str "Invalid config:\n"
                    (with-out-str (pprint err))))))
    (let [full-config (merge snék.config/default-config u-config)]
      (if-some [err (validate-first-problem full-config :snék.config/config)]
        (throw (java.lang.IllegalArgumentException.
                 (str "Config doesn't satisfy constraint:\n"
                      (with-out-str (pprint err))))))
      full-config)))

(defn -main [& args]
  (let [{:keys [options arguments
                errors summary]} (cli/parse-opts args cli-options)]
    (if (:help options)
      (do (println (usage summary))
          (System/exit 0)))
    (let [config (load-and-validate-config (:config-file options))]
      (snék.game/launch config))))

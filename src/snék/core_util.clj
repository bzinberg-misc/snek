(ns snék.core-util
  "Utilities for `snék.core`.  Roughly speaking, we'll call a piece of
  supporting code a 'utility' if it is self-contained and general-purpose
  enough that it could be used by projects quite different from this one."
  (:require [clojure.spec.alpha :as s]))

(def _2pi (* 2 Math/PI))
(def _halfpi (* 0.5 Math/PI))

(defmacro arr1
  "Create a singleton array of type `cls`.`"
  [cls & ctor-arg-exprs]
  `(into-array ~cls [(new ~cls ~@ctor-arg-exprs)]))

(defn get-only
  "Asserts that `coll` has exactly one element, and returns that element."
  [coll]
  (assert (= 1 (bounded-count 2 coll)))
  (first coll))

(defn zip
  "Zip iterator, equivalent to `(map vector first-seq & more-seqs)`."
  [first-seq & more-seqs]
  (apply map vector first-seq more-seqs))

(defn contains-all?
  "Returns true if the map `m` contains all elements of `ks` as keys, returns
  false otherwise."
  [m ks]
  (every? #(contains? m %1) ks))

(s/fdef last-index :args (s/cat :v vector?))
(defn last-index
  "Returns the index of the last element in vector `v`, that is, count(v) - 1."
  [v]
  (- (count v) 1))

(defn avg
  "Returns the arithmetic mean of the arguments: avg(x1, ..., xn) =
  (x1 + ... + xn) / n."
  [x & xs]
  (/ (apply + x xs)
     (inc (count xs))))

(defn min-max
  "Returns the vector `[min(x, ...), max(x, ...)]`, where `...` denotes the
  varargs."
  [x & xs]
  [(apply min x xs)
   (apply max x xs)])

(s/fdef update-vec-last :args (s/cat :v vector? :rest (s/+ any?)))
(defn update-vec-last [v f & args]
  (apply update v (last-index v) f args))

(defn vector-butlast
  "Like `butlast`, but requires the input to be a vector, and returns a vector.
  If `n` is supplied, the last `n` elements are dropped instead of just the
  last 1 element (like `drop-last`).  This function uses `subvec`, so it is
  much more performant than the general sequence case."
  ([v] (vector-butlast v 1))
  ([v n]
   (subvec v 0 (max 0
                    (- (count v) n)))))

(defn sq
  "Squaring function: sq(x) = x*x."
  [x] (* x x))

(s/fdef residue-just-above
        :args (s/cat :x number? :minval number? :modulus pos?))
(defn residue-just-above
  "Returns the least (closest to negative infinity) number greater than or
  equal to `minval` that can be expressed as `x + n * modulus` for some integer
  `n`.  Modulus must be positive."
  [x minval modulus]
  (+ minval
     (mod (- x minval) modulus)))

(defn scalev
  "Scale vector `v` by scalar `a`."
  [a v]
  (mapv #(* a %1) v))

(defn +v
  "Vector addition.  All arguments must be finite seqables of the same length,
  whose entries are numbers.  Returns a vector."
  [v & vs]
  (apply mapv + v vs))

(defn -v
  "Vector subtraction.  All arguments must be finite seqables of the same
  length, whose entries are numbers.  Returns a vector."
  [v & vs]
  (apply mapv - v vs))

(defn halfv [v] (scalev 1/2 v))

(defn dot
  "Dot product: v1 . v2 = sum_i (v1[i] * v2[i])."
  [v1 v2]
  (apply + (map * v1 v2)))

(comment
  (dot [1 2] [3 4])
  ; -> 11
  )

(defn normsq
  "Squared L^2 norm: |v|^2 = sum_i (v_i^2)."
  [v] (dot v v))

(defn norm
  "L^2 norm: |v| = sqrt(sum_i (v_i^2))."
  [v] (Math/sqrt (normsq v)))

(defn e-dist
  "L^2 distance: dist(a, b) = sqrt(sum_i (a_i - b_i))."
  [pt1 pt2]
  (norm (-v pt1 pt2)))

(defn h-dist
  "Hyperbolic distance between `pt1` and `pt2` in the Poincaré disk.

  This function computes the distance directly, without computing the
  parameters of the hyperbolic line containing `pt1` and `pt2`.

  See:
  https://en.wikipedia.org/w/index.php?title=Poincar%C3%A9_disk_model&oldid=1193367466#Lines_and_distance
  "
  [pt1 pt2]
  (let [pt1-normsq (normsq pt1)
        pt2-normsq (normsq pt2)]
    (* 2 (Math/log (/ (+ (norm (-v pt1 pt2))
                         (Math/sqrt (+ (* pt1-normsq pt2-normsq)
                                       (* -2 (dot pt1 pt2))
                                       1)))
                      (Math/sqrt (* (- 1 pt1-normsq)
                                    (- 1 pt2-normsq))))))))

(defn unit
  "Normalizes `v` to a unit vector: unit(v) = v / norm(v)."
  [v] (scalev (/ (norm v))
              v))

(defn matvecmul-2x2
  "Returns the matrix-vector product Av."
  [[a b
    c d :as A]
   [x y :as v]]
  [(+ (* a x) (* b y))
   (+ (* c x) (* d y))])

(defn mat-inv-2x2
  "Returns the matrix inverse of `A`."
  [[a b
    c d :as A]]
  (let [det-A (- (* a d) (* b c))]
    (mapv #(/ % det-A) [   d  (- b)
                        (- c)    a])))

(defn matvecdiv-2x2
  "Returns the left-quotient A\\v = A^{-1} v, where `A` is a 2x2 matrix and
  `v` a vector of length 2.  This is also known as a 'linear solve,' as the
  returned value is a solution `x` to the equation `Ax = v`.

  Note: This is an unoptimized hand-implementation.  It probably doesn't have
  as good numeric stability as a sophisticated implementation."
  [A v]
  (matvecmul-2x2 (mat-inv-2x2 A)
                 v))

(s/def ::non-neg (s/or :zero zero? :pos pos?))
(s/fdef satisfies-triangle-inequality :args (s/cat :a ::non-neg
                                                   :b ::non-neg
                                                   :c ::non-neg))
(defn satisfies-triangle-inequality?
  "Returns true if a, b and c satisfy the triangle inequality, i.e., can be
  the sides of a Euclidean triangle.

  Allows degenerate triangles like `a + b == c`"
  [a b c]
  (and (>= (+ a b) c)
       (>= (+ a c) b)
       (>= (+ b c) a)))

(s/fdef triangle-angle-from-side-lengths :args (s/cat :a ::non-neg
                                                      :b ::non-neg
                                                      :c ::non-neg))
(defn triangle-angle-from-side-lengths
  "Computes the measure (in radians, nonnegative) of the angle between the first
  two sides (`a` and `b`) in a Euclidean triangle with side lengths `a, b, c`."
  [a b c]
  ;; Using the law of cosines
  (Math/acos (/ (+ (sq a)
                   (sq b)
                   (- (sq c)))
                (* 2 a b))))

(s/fdef in-range-mod?
        :args (s/cat :x number? :low number? :high number? :modulus pos?))
(defn in-range-mod?
  "Returns true if there exists an integer n such that
       low <= x + n * modulus <= high
  Requires `low <= high`, and `modulus` must be positive.

  TODO test this function"
  [x low high modulus]
  (assert (pos? modulus))
  (assert (<= low high))
  (<= (mod (- x low) modulus)
      (- high low)))

(s/fdef scalar-cross
        :args (s/cat :v1 ::vec2d :v2 ::vec2d)
        :ret number?)
(defn scalar-cross
  "Returns the scalar `z` such that append(v1, 0) × append(v2, 0) = (0, 0, z).
  Here `×` is the cross product on R^3."
  [v1 v2]
  ;; FIXME: find a way to meaningfully unit-test this function
  (let [[a b] v1
        [c d] v2]
    (- (* a d) (* b c))))

(defn angle-of
  "Planar angle of the given vector [x y] (in radians).  That is, atan2(y, x)."
  [[x y]]
  (Math/atan2 y x))

(defn to-polar
  "Returns the polar coordinates (r, θ) of the point v = (x, y)."
  [v] [(norm v)
       (angle-of v)])

(defn cossin
  "Returns the vector `[cos(t), sin(t)]`.
  Implemented naively, by computing sin and cos separately."
  [t] [(Math/cos t)
       (Math/sin t)])

(defn rand-from-unit-disk
  "Returns a point sampled uniformly from the unit disk."
  [] (let [θ (rand _2pi)
           r (Math/sqrt (rand))]
       (scalev r (cossin θ))))

(defn ±
  "Returns the vector [a+b, a-b].  Note that this order is not guaranteed to
  be descending, as b need not be positive."
  [a b]
  [(+ a b) (- a b)])

(defn ∓
  "Returns the vector [a-b, a+b].  Note that this order is not guaranteed to
  be ascending, as b need not be positive."
  [a b]
  [(- a b) (+ a b)])

(s/fdef quarter-turns
        :args (s/cat :v ::vec2d :n integer?))
(defn quarter-turns
  "Applies `n` quarter-turns to the vector `v`.  The integer `n` need not be
  positive.  A single quarter-turn is rotation by an angle of pi/2 in the
  positive direction (in a right-handed coordinate system this is
  counter-clockwise)."
  [v n]
  (let [[x y] v]
    (case (mod n 4)
      0 v
      1 [(- y)    x]
      2 [(- x) (- y)]
      3 [   y  (- x)])))

; Clearer names than the Java ones (_what_ to radians?)
(defn deg->rad [x] (Math/toRadians x))
(defn rad->deg [x] (Math/toDegrees x))
; Clojure wrappers, as one can't use `apply` on Java static methods
(defn hypot [x y] (Math/hypot x y))

(ns snék.extra-spec
  "Specs that don't have an obvious place to go without creating dependency
  cycles."
  (:require [clojure.spec.alpha :as s]
            [snék.core-util :refer :all]
            [snék.core :as c :refer :all]
            [snék.config :as config]))

(s/def ::point-inside-fence
  ;; Point in the plane (`::point2d`) that lies strictly inside the fence
  ;; (i.e., the disk of radius `config/FENCE_RADIUS` centered at the origin).
  (s/and ::c/point2d
         #(< (normsq %1)
             ; All we know statically about fence radius is that it's less than
             ; 1.
             1)))

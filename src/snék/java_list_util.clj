(ns snék.java-list-util
  "Utilities for operating on Java `List`s"
  (:refer-clojure :exclude (last butlast))
  (:import (java.util List)))

(defn update!
  "Like `clojure.core/update`, but mutates `lst` in place rather than returning
  a new list.  Returns the old value of the `lst[idx]`."
  [^List lst idx f & args]
  (.set lst idx
        (apply f (.get lst idx) args)))

(defn update-chainable!
  "Like `update!`, but returns `lst` (which will have been mutated)."
  [^List lst idx f & args]
  (apply update! lst idx f args)
  lst)

(defn last
  "Returns the last element of `lst`.  Requires that `lst` is nonempty."
  [^List lst]
  (.get lst (dec (count lst))))

(defn update-last!
  "Equivalent to `update!` with the `idx` argument set to the index of the last
  element of `lst`.  Requires that `lst` is nonempty."
  [^List lst f & args]
  (apply update!
         lst
         (dec (count lst))
         f
         args))

(defn append!
  "Appends `elt` to `lst`."
  [^List lst elt]
  (.add lst elt))

(defn butlast
  "Like `butlast`, but requires the input to be a `List`, and returns a `List`.
  If `n` is supplied, the last `n` elements are dropped instead of just the
  last 1 element (like `drop-last`).

  NOTE: This function calls `.subList`, so if `lst` is mutable, then mutations
  to `lst` will be seen by the returned sublist and vice versa."
  ([^List lst] (butlast lst 1))
  ([^List lst n]
   (.subList lst 0 (max 0
                        (- (count lst) n)))))

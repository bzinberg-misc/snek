(ns snék.config
  "Game configuration."
  (:require [clojure.spec.alpha :as s]
            [snék.core-util :as u])
  (:import (javafx.scene.input KeyCode)))

;; ====== DEFAULT CONFIG MAP ======

(def default-config
  {::snake-initial-speed-dist-per-sec 0.5
   ::snake-accel-dist-per-sec2 0.005
   ::snake-length-zero-apples 1.5
   ::snake-growth-per-apple 0.5
   ::fence-h-radius (Math/log 19)
   ::fence-h-padding (- (Math/log 19) (Math/log 93/7))
   ::apple-collision-h-radius 0.1
   ::turn-keycodes {KeyCode/LEFT :pos
                    KeyCode/RIGHT :neg}
   ::canvas-size-percent 75.})

;; ====== CONFIG SCHEMA ======

;; ==== Game configuration ====
;; I.e., stuff that's intrinsic to the game, not the GUI.

(s/def ::snake-initial-speed-dist-per-sec
  ; Initial speed of the snake's motion in hyperbolic length units per second.
  ::u/non-neg)

(s/def ::snake-accel-dist-per-sec2
  ; Acceleration of the snake's motion in hyperbolic length units per second
  ; per second.
  ::u/non-neg)

(s/def ::snake-length-zero-apples
  ; Maximum hyperbolic length (i.e., length that the snake will eventually grow
  ; to) of a snake that has eaten zero apples.
  pos?)

(s/def ::snake-growth-per-apple
  ; Number of hyperbolic length units by which the snake grows each time it
  ; eats an apple.  (The growth is not instantaneous; instead, the tail stays
  ; in place while the head moves until the new length is achieved.)
  ::u/non-neg)

(s/def ::fence-h-radius
  ; Hyperbolic radius of the circular 'fence' centered at the origin.  The
  ; player loses the game if the snake crashes into the fence.
  pos?)

(s/def ::fence-h-padding
  ; Minimum allowed hyperbolic distance from the fence to an apple location.
  ; This constant exists to prevent the apple from appearing in a location that
  ; is unreasonably difficult to get to without crashing into the fence.  Must
  ; be less than `::fence-h-radius`, of course.
  ::u/non-neg)

(s/def ::apple-collision-h-radius
  ; Hyperbolic radius of the collision region of the apple (a circle whose
  ; hyperbolic center is the location of the apple).
  pos?)

(s/def ::turn-keycodes
  ; Game controls: mapping from keycode to turn direction.
  (s/map-of #(instance? KeyCode %1)
            #{:pos :neg}
            :conform-keys true))


;; ==== GUI configuration ====

(s/def ::canvas-size-percent
  ;; Linear size of the entire JavaFX canvas, as a percentage of the size of
  ;; the user's primary screen, that is, of the smaller of the two bounds in
  ;;
  ;;     javafx.stage.Screen.getPrimary().getVisualBounds()
  ;;
  ;; The canvas is a square, and this linear size is the square's width and
  ;; height.
  #(< 0 % 100))

(s/def ::region-size
  ;; Width and height (in that order) of a rectangular region, in world units
  ;; (not pixels).
  (s/coll-of pos? :count 2))

(s/def ::region-center
  ;; Location of the center of a rectangular region, in world coordinates.
  (s/coll-of number? :count 2))

(s/def ::region-size-px
  ;; Width and height (in that order, not necessarily integer-valued) of a
  ;; rectangular region of the JavaFX canvas, in pixels.
  (s/coll-of pos? :count 2))

(s/def ::region-center-px
  ;; Location of the center of a rectangular region of JavaFX canvas, in JavaFX
  ;; coordinates: +x points right, +y points down, and (0, 0) is the top-left
  ;; corner of the canvas.  The length unit is pixels but the coordinates are
  ;; not necessarily integer-valued.
  (s/coll-of pos? :count 2))

;; ==== Multi-field constraints ====

(s/def ::user-config
  ;; Keys in the config map that can be ovverridden by a user-supplied config.
  (s/keys :opt [::snake-initial-speed-dist-per-sec
                ::snake-accel-dist-per-sec2
                ::snake-length-zero-apples
                ::snake-growth-per-apple
                ::fence-h-radius
                ::fence-h-padding
                ::apple-collision-h-radius
                ::canvas-size-percent]))

(s/def ::config
  (s/and
    #(< (::fence-h-padding %1)
        (::fence-h-radius %1))
    (s/keys :req [::snake-initial-speed-dist-per-sec
                  ::snake-accel-dist-per-sec2
                  ::snake-length-zero-apples
                  ::snake-growth-per-apple
                  ::fence-h-radius
                  ::fence-h-padding
                  ::apple-collision-h-radius
                  ::canvas-size-percent
                  ::turn-keycodes])))


;; ====== CANVAS LAYOUT ======

(s/def ::jfx-region
  ;; A rectangular (axis-aligned) region of the JavaFX canvas.
  (s/and
    (s/keys :req [::region-size
                  ::region-center
                  ::region-size-px
                  ::region-center-px])
    ; The JavaFX coordinates of a point in the canvas are always nonnegative,
    ; so the center must be at least `region-size-px / 2` units away from zero
    #(every? (map >= (::region-center-px %1)
                  (u/halfv (::region-size-px %1))))))

(defn _twice [x]
  [x x])

(s/fdef make-view-region-main :args (s/cat :size-px pos?)
        :ret ::jfx-region)
(defn make-view-region-main
  "Returns the region corresponding to the main (first-person) view, assuming
  that the canvas width and height are both `size-px` pixels."
  [size-px]
  {::region-size [2.0 2.0]
   ::region-center [0. 0.]
   ::region-size-px [size-px size-px]
   ::region-center-px (_twice (* 1/2 size-px))})

(s/fdef make-view-region-pip :args (s/cat :size-px pos?)
        :ret ::jfx-region)
(defn make-view-region-pip
  "Returns the region corresponding to the picture-in-picture view, assuming
  that the canvas width and height are both `size-px` pixels."
  [size-px]
  {::region-size [2.0 2.0]
   ::region-center [0. 0.]
   ::region-size-px (_twice (* 1/8 size-px))
   ::region-center-px (_twice (* 15/16 size-px))})


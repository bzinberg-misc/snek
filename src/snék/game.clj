(ns snék.game
  "Gameplay-related utilities.

  I.e., things that are not purely geometry but instead contain some logic that
  is specific to the snake game (snake body data structure, movement of the
  head and tail, etc.)."
  (:require [clojure.spec.alpha :as s]
            [snék.core :as c :refer :all]
            [snék.core-util :refer :all]
            [snék.java-list-util :as jlist]
            [snék.draw :refer :all]
            [snék.config :as config]
            [snék.extra-spec :as es]
            [snék.mobius :as m])
  (:import (javafx.animation Timeline KeyFrame KeyValue)
           (javafx.application Application Platform)
           (javafx.beans.property SimpleObjectProperty)
           (javafx.beans.value ChangeListener)
           (javafx.collections FXCollections ListChangeListener)
           (javafx.event ActionEvent EventHandler)
           (javafx.scene Scene Group Node)
           (javafx.scene.control Dialog ButtonType ButtonBar$ButtonData)
           (javafx.scene.input KeyEvent KeyCode)
           (javafx.scene.paint Color)
           (javafx.stage Screen Stage)
           (javafx.util Duration)))

(def the-config
  "Global configuration for the game.  May be modified during game init, as a
  user-supplied config may be loaded and merged in."
  (atom config/default-config))

(def the-config-derived
  "Implementation detail.  Map containing precomputed values derived from
  `the-config`."
  (atom nil))

(defn h->e-radius-at-origin
  "Returns the Euclidean radius of the circle centered at the origin whose
  hyperbolic radius is `h-radius`."
  [h-radius]
  (let [[e-center e-radius] (h->e-circle [0 0] h-radius)]
    e-radius))

(s/def ::fence-e-radius
  ;; Euclidean radius of the fence, whose center (both Euclidean and
  ;; hyperbolic) is the origin.
  #(< 0 % 1))

(s/def ::apple-domain-e-radius
  ;; Euclidean radius of the locus of possible apple locations, whose center is
  ;; the origin.  Its hyperbolic radius is
  ;; `fence-h-radius - fence-h-padding`.
  #(< 0 % 1))

(defn compute-derived-config
  [config]
  {::fence-e-radius (h->e-radius-at-origin
                      (::config/fence-h-radius config))
   ::apple-domain-e-radius (h->e-radius-at-origin
                             (- (::config/fence-h-radius config)
                                (::config/fence-h-padding config)))})

(s/def ::jfx-node
  ;; JavaFX `Arc` node representing a hyperbolic line or line segment.  Must
  ;; not be nil.  The infinite-radius case (diameters of the disk) is currently
  ;; not supported.
  (s/and some? (partial instance? Node)))

(s/def ::millis-since-game-start
  ;; Timestamp represented as the number of milliseconds since the start of the
  ;; current game.
  nat-int?)

(s/def ::creation-time-millis
  ;; Time at which a snake segment (see `::snake-seg`) was first created, in
  ;; milliseconds since the start of the current game.  Stays the same even as
  ;; the segment grows or shrinks as the snake "slithers."
  ::millis-since-game-start)

(s/def ::orig-start-angle
  ;; Start angle (see `::c/start-angle`) that a snake segment (see
  ;; `::snake-seg`) had at the time it was first created.  Stays the same even
  ;; as the current start angle of the snake segment changes, as the snake
  ;; "slithers."
  ;;
  ;; Design note: The reason we keep track of this value is that it permits us
  ;; to directly compute the location of the snake's head at a given time: the
  ;; head is `∫(slither speed) dt` hyperbolic length units away from the point
  ;; with absolute angle `orig-start-angle` on the frontmost snake segment
  ;; (where the integral is taken over the interval from `creation-time-millis`
  ;; to the current time).  Because the snake's tail may move away from
  ;; `orig-start-angle`, it is not sufficient to keep track of just the current
  ;; `start-angle`.
  ::c/absolute-angle)

(s/def ::seg-id
  ;; Stable identifier for an individual segment of the snake.  As the segment
  ;; grows and shrinks over time while the snake slithers, this identifier will
  ;; stay the same.
  any?)

(s/def ::snake-seg
  ;; "Snake segment," which includes not only a directed hyperbolic line
  ;; segment (`::c/seg`), but also further gameplay-related context about the
  ;; segment (`::seg-id`, `::creation-time-millis` and `::orig-start-angle`).
  (s/keys :req [::seg-id
                ::c/seg
                ::creation-time-millis
                ::orig-start-angle]))

(s/def ::snake-segs
  ;; State representation for the entire snake.  The representation is a
  ;; nonempty `ObservableList` of `::snake-seg`s, in order, with the first
  ;; element of the `ObservableList` being the rearmost segment of the snake
  ;; and the last element being the segment that contains the snake's head.
  (s/coll-of ::snake-seg :min-count 1))

(s/def ::arrowhead
  ;; Arrowhead displayed at the head of the snake as a visual aid to the
  ;; player.  Points in the direction the snake is traveling.
  ::jfx-node)

(s/def ::h-center
  ;; Hyperbolic center of a hyperbolic circle.
  ::c/point-inside-disk)

(s/def ::e-center
  ;; Euclidean center of a hyperbolic circle.
  ::c/point-inside-disk)

(s/def ::e-radius
  ;; Euclidean radius of a hyperbolic circle.
  pos?)

(s/def ::apple
  ;; Location and collision region for an apple, in world coordinates.
  ;;
  ;; If the snake's head enters the collision region, the snake is considered
  ;; to have eaten the apple.
  ;;
  ;; The location of the apple is `h-center`, and the collision region is a
  ;; hyperbolic circle centered at `h-center`.  The Euclidean center and
  ;; Euclidean radius of the collision region are `e-center` and `e-radius`.
  ;; The hyperbolic radius is currently not stored (though it can be computed
  ;; from `e-center` and `e-radius`).
  (s/and
    (s/keys :req [::h-center ::e-center ::e-radius])
    ; The circle must lie entirely within the Poincaré disk:
    #(< (+ (norm (::e-center %1))
           (::e-radius %1))
        1)))

(defn snake-length-after-apples
  "Returns the maximum hyperbolic length (i.e., length that the snake will
  eventually grow to) of a snake that has eaten `n` apples."
  [n]
  (+ (::config/snake-length-zero-apples
       @the-config)
     (* n (::config/snake-growth-per-apple
            @the-config))))

(defn rand-apple-location
  "Returns a random point drawn uniformly (with respect to Euclidean area, for
  ease of implementation) from the region of allowed apple locations, i.e., the
  disk of Euclidean radius `apple-domain-e-radius` centered at the origin."
  []
  (scalev (::apple-domain-e-radius @the-config-derived)
          (rand-from-unit-disk)))

(s/fdef make-apple :args (s/cat :apple-location ::es/point-inside-fence))
(defn make-apple
  "Creates an apple (`::apple`) at the given location.  (Does not modify game
  state or display anything, just returns a data structure.)"
  [apple-location]
  (let [[e-center e-radius] (h->e-circle apple-location
                                         (::config/apple-collision-h-radius
                                           @the-config))]
    {::h-center apple-location
     ::e-center e-center
     ::e-radius e-radius}))

(s/fdef update-end-pt-from-motion
        :args (s/cat :snake-seg ::snake-seg
                     :now-millis ::millis-since-game-start))
(defn update-end-pt-from-motion
  "Changes the start and end points of `(::c/seg snake-seg)` as follows:

  - The `::c/start-angle` (which determines the start point of the arc) is
    set to `orig-start-angle`.
  - The length (`::c/h-length`) is set to the distance traversed by the snake
    between `creation-time-millis` and `now-millis`, as determined by
    constant-accelation motion with the initial conditions
    `::config/snake-initial-speed-dist-per-sec` and
    `::config/snake-accel-dist-per-sec2`.
  - The direction (`::c/angle-dir`) is preserved.
  - The segment id (`::seg-id`) is preserved.

  You will usually want to follow this function with a call to
  `trim-snake-tail!`, as this function does not enforce any maximum on the
  snake's length."
  [{::c/keys [seg]
    ::keys [creation-time-millis orig-start-angle]
    :as snake-seg}
   now-millis]
  (let
    [angle-dir (if (pos? (::c/delta-angle seg)) :pos :neg)
     avg-velocity-dist-per-sec (+ (::config/snake-initial-speed-dist-per-sec
                                    @the-config)
                                  (* 1e-3
                                     (::config/snake-accel-dist-per-sec2
                                       @the-config)
                                     (avg creation-time-millis
                                          now-millis)))
     new-h-length (* 1e-3
                     avg-velocity-dist-per-sec
                     (- now-millis creation-time-millis))
     new-seg (-> seg
                 (dissoc ::c/start-angle ::c/delta-angle ::c/h-length)
                 (assoc ::c/start-angle orig-start-angle
                        ::c/h-length new-h-length)
                 (seg-from-start-length ::c/angle-dir angle-dir))]
    (assoc snake-seg ::c/seg new-seg)))

(s/fdef update-snake-head! :args (s/cat :snake-segs ::snake-segs
                                        :now-millis ::millis-since-game-start))
(defn update-snake-head!
  "Updates the end-point of newest snake segment (the one containing the
  snake's head) according to `update-end-pt-from-motion`."
  [snake-segs now-millis]
  (jlist/update-last! snake-segs
                      update-end-pt-from-motion
                      now-millis))

(s/def ::rear-seg-idx
  ;; Given a `::snake-segs` and a prescribed `max-h-length`, the
  ;; `::rear-seg-idx` is the index of the rearmost segment of the result of
  ;; cutting the snake to total hyperbolic length at most `max-h-length`.
  ;; In other words, `rear-seg-idx` is the largest index such that
  ;;
  ;;     (-> (subvec snake-segs rear-seg-idx)`
  ;;         (map-in [::c/seg ::c/h-length])
  ;;         #(apply + %1))
  ;;
  ;; is at most `max-h-length`.
  nat-int?)

(s/def ::rear-seg-h-length
  ;; In the context of `::rear-seg-idx`, `::rear-seg-h-length` is the
  ;; hyperbolic length that `(snake-segs rear-seg-idx)` would have to shrink to
  ;; in order to make the total length of the snake exactly `max-h-length`.  In
  ;; other words, this value is equal to
  ;;
  ;;     (- max-h-length
  ;;        (apply + (-> (subvec snake-segs (inc rear-seg-idx))
  ;;                     (map-in [::c/seg ::c/h-length]))))
  ;;
  ;; If this key is absent, it means that the total hyperbolic length of
  ;; `(subvec snake-segs rear-seg-idx)` already doesn't exceed `max-h-length`,
  ;; i.e., the rear segment's length doesn't need to change.
  pos?)

(s/fdef snake-head-pt
        :args (s/cat :snake-segs ::snake-segs)
        :ret ::c/point-inside-disk)
(defn snake-head-pt
  "Returns the location of the snake's head."
  [snake-segs]
  (arc-end-pt (::c/seg (jlist/last snake-segs))))

(s/fdef find-snake-rear
        :args (s/cat :snake-segs ::snake-segs
                     :max-h-length pos?)
        :ret (s/keys :reg [::rear-seg-idx]
                     :opt [::rear-seg-h-length]))
(defn find-snake-rear
  "Finds the index of the rearmost segment in `snake-segs`, as described in
  the spec docs of `::rear-seg-idx` and `::rear-seg-h-length`."
  [snake-segs max-h-length]
  (assert (seq snake-segs) "`snake-segs` must be nonempty")
  (let [last-seg-length (get-in (jlist/last snake-segs)
                                [::c/seg ::c/h-length])]
    (cond
      (< max-h-length last-seg-length) {::rear-seg-idx (last-index snake-segs)
                                        ::rear-seg-h-length max-h-length}
      (= max-h-length last-seg-length) {::rear-seg-idx (last-index snake-segs)}
      (= 1 (count snake-segs)) {::rear-seg-idx 0}
      :else (recur (jlist/butlast snake-segs)
                   (- max-h-length last-seg-length)))))

(s/fdef head-body-crash? :args (s/cat :snake-segs ::snake-segs))
(defn head-body-crash?
  "Returns true if the snake's head intersects its body.  That is, returns true
  if the last snake segment (the one containing the snake's head) intersects
  with any of the other segments other than the second-to-last.  (The last
  segment always intersects with the second-to-last segment at its endpoint;
  a crash is when there are more intersections.)"
  [snake-segs]
  (not-every? empty? (map arc-intersections
                          (repeat (::c/seg (jlist/last snake-segs)))
                          (map ::c/seg (jlist/butlast snake-segs 2)))))

(s/fdef head-outside-fence? :args (s/cat :snake-segs ::snake-segs))
(defn head-outside-fence?
  "Returns true if the snake's head is on or outside the fence."
  [snake-segs]
  (>= (normsq (snake-head-pt snake-segs))
      (sq (::fence-e-radius @the-config-derived))))

(s/fdef head-near-apple? :args (s/cat :snake-segs ::snake-segs
                                      :apple ::apple))
(defn head-near-apple?
  "Returns true if the snake's head is inside the collision region of the
  apple."
  [snake-segs apple]
  (< (e-dist (snake-head-pt snake-segs)
             (::e-center apple))
     (::e-radius apple)))

(defn remove-jfx-node!
  "Removes the given JavaFX node from its parent, if it has a parent."
  [^Node jfx-node]
  (if-some [parent (.getParent jfx-node)]
    (.. parent (getChildren) (remove jfx-node))))

(s/fdef trim-snake-tail!
        :args (s/cat :snake-segs ::snake-segs
                     :max-h-length pos?))
(defn trim-snake-tail!
  "Mutates `snake-segs` by removing elements from the front of the list
  (= removing segments from the rear of the snake) and moving the start point
  of the new first element of the list (= the new rear segment of the snake)
  so that the total length of the snake is at most `max-h-length`.  Does
  not change any of the non-rear segments of the snake, and if `snake-segs`
  contains only one segment, does not move the end-point of that segment."
  [snake-segs max-h-length]
  (let [{::keys [rear-seg-idx
                 rear-seg-h-length]} (find-snake-rear
                                       snake-segs
                                       max-h-length)
        segs-to-discard (.subList snake-segs 0 rear-seg-idx)
        new-segs (cond-> (.subList snake-segs
                                   rear-seg-idx
                                   (count snake-segs))
                   (some? rear-seg-h-length)
                   (jlist/update-chainable! 0
                                            update
                                            ::c/seg
                                            change-length-moving-start-pt
                                            rear-seg-h-length))]
    (.clear segs-to-discard)))

(s/fdef update-snake-head-and-tail!
        :args (s/cat :snake-segs ::snake-segs
                     :now-millis ::millis-since-game-start
                     :max-h-length pos?))
(defn update-snake-head-and-tail!
  "Updates the snake head position as in `update-snake-head!` and then trims
  the snake's tail as in `trim-snake-tail!`."
  [snake-segs now-millis max-h-length]
  (update-snake-head! snake-segs now-millis)
  (trim-snake-tail! snake-segs max-h-length))

(s/fdef add-snake-seg-from-turn!
        :args (s/cat :snake-segs ::snake-segs
                     :turn-dir ::angle-dir
                     :now-millis ::millis-since-game-start
                     :new-seg-id ::seg-id))
(defn add-snake-seg-from-turn!
  "Adds a new snake segment to `snake-segs`, with id `new-seg-id`, creation
  time `now-millis`, turn direction as in `turn-from-seg`, and length a very
  small nonzero value.

  TODO: Test this function"
  [snake-segs turn-dir now-millis new-seg-id]
  (let [{prev-seg ::c/seg} (jlist/last snake-segs)
        seg (turn-from-seg prev-seg 1e-4 turn-dir)]
    (jlist/append! snake-segs {::seg-id new-seg-id
                               ::c/seg seg
                               ::creation-time-millis now-millis
                               ::orig-start-angle (::c/start-angle seg)})))

(defn change-to-new-apple!
  "Replaces the current apple (an `ObservableObjectValue`) with a new apple at
   a (Euclidean-)uniformly random location inside the fence.  Updates
  `num-apples-eaten` (integer-valued atom) by applying `update-num-fn` (default:
  `inc`)."
  [cur-apple num-apples-eaten
   & {:keys [update-num-fn] :or {update-num-fn inc}}]
  (swap! num-apples-eaten update-num-fn)
  (.setValue cur-apple (make-apple (rand-apple-location))))

(s/fdef make-initial-snake-segs :args (s/cat
                                        :now-millis ::millis-since-game-start))
(defn make-initial-snake-segs
  "Creates the initial snake body (`::snake-segs`).  Everything about the
  initial snake body is hard-coded except for (i) the creation timestamp, which is
  supplied by the argument `now-millis`, and (ii) the ids (`::seg-id`) for the
  segments, which are produced by calling `gen-next-seg-id` once per segment."
  [now-millis gen-next-seg-id]
  (let [center [-2. -1.]
        seg-id (gen-next-seg-id)
        seg (-> center
                (h-line)
                (merge {::c/start-angle (/ Math/PI 6)
                        ::c/h-length 1e-4})
                (seg-from-start-length ::c/angle-dir :pos))]
    [{::seg-id seg-id
      ::c/seg seg
      ::creation-time-millis now-millis
      ::orig-start-angle (::c/start-angle seg)}]))

(defn reset-snake!
  "Replaces the contents of `snake-segs` (an `ObservableList` of
  `::snake-segs`) with a new snake (see `make-initial-snake-segs`)."
  [snake-segs now-millis gen-next-seg-id]
  (doto snake-segs
    ; This change could be written as `.setAll(...)`, but we separate it out
    ; into the two operations `.clear()` and `.addAll(...)` because this gives
    ; us the convenience of not having to add a code path to the
    ; `ListChangeListener` to handle `setAll`.
    (.clear)
    (.addAll (into-array (make-initial-snake-segs
                           now-millis gen-next-seg-id)))))

(defn make-snake-jfx-fixed
  "Returns the pair `[snake-jfx-group handle-snake-change!]`, where:

  * `handle-snake-change!` is a `ListChangeListener` that expects to be
    attached to an `ObservableList` of `::snake-segs`

  * `snake-jfx-group` is a JavaFX `Group` that is managed by the event
    handler in `handle-snake-change!` so that it contains the graphical
    representation of the (up-to-date) snake state as specified by the
    `ObservableList` it's listening to.  The graphical representation
    exists within the region `jfx-region` and contains both the arcs of the
    snake's body (`::seg`) and the arrowhead at the snake's head
    (`::arrowhead`).  The location of the camera is fixed, i.e. does not follow
    the snake's movement; it stays centered at the origin (in world
    coordinates)."
  [jfx-region]
  (let
    [; Within this scope, let's say that "snake body" includes just the
     ; segments (arcs), not the arrowhead.
     snake-body-jfx-group (Group.)
     body-jfx-nodes (atom {})
     arrowhead (make-arrowhead-node)
     snake-jfx-group (doto (Group.)
                       (.. (getChildren) (addAll [snake-body-jfx-group
                                                  arrowhead])))
     handle-snake-change!
     (reify ListChangeListener
       (onChanged [this change]
         (while (.next change)
           (assert (not (.wasPermutated change)))
           ; We do not do any operations that we expect to trigger "Update"
           ; events.  Setting individual elements triggers a "Replace" event,
           ; not an "Update" event.
           (assert (not (.wasUpdated change)))
           (cond
             (.wasReplaced change)
             ; Currently, the "replaced" event only occurs when we call
             ; `.set(...)` on the `ObservableList` to change a single
             ; snake segment, i.e., replace it with a segment having the
             ; same `::seg-id`.  So here we update the JavaFX `Arc`
             ; corresponding to that segment to match the new data.
             (let [{::keys [seg-id]
                    ::c/keys [seg]} (get-only (.getAddedSubList change))]
               (set-jfx-arc-angles-from-seg! (@body-jfx-nodes seg-id) seg)
               (if (= (.getTo change) (count (.getList change)))
                 (move-arrowhead-to-arc-end-pt! arrowhead seg jfx-region)))

             (.wasRemoved change)
             ; One or more segments was removed from the snake.  First
             ; remove the jfx nodes for the removed segments from the
             ; display, then delete the corresponding entries in
             ; `body-jfx-nodes`.
             (let [removed-seg-ids (map ::seg-id (.getRemoved change))]
               (dorun (map (comp remove-jfx-node! @body-jfx-nodes)
                           removed-seg-ids))
               (apply swap! body-jfx-nodes dissoc removed-seg-ids))

             (.wasAdded change)
             ; One or more segments were added to the snake.  Create a jfx node
             ; for each new segment, and assoc these new nodes to
             ; `body-jfx-nodes`.  If one of the added nodes is the snake's neck
             ; (last element of the `ObservableList`), move the arrowhead to
             ; the new head's location.
             (do (doseq [{::keys [seg-id]
                          ::c/keys [seg]} (.getAddedSubList change)]
                   (let [arc (make-seg-node seg jfx-region)]
                     (.. snake-body-jfx-group (getChildren) (add arc))
                     (swap! body-jfx-nodes assoc seg-id arc)))
                 (if (= (.getTo change) (count (.getList change)))
                   (move-arrowhead-to-arc-end-pt!
                     arrowhead
                     (-> change (.getList) (jlist/last) (::c/seg))
                     jfx-region)))))))]
    [snake-jfx-group handle-snake-change!]))

(defn make-apple-jfx-fixed
  "Returns the pair `[apple-jfx-group handle-apple-change!]`, where:

  * `handle-apple-change!` is a `ChangeListener` that expects to be
    attached to an `ObservableObjectValue` with an `::apple` as its
    underlying value.

  * `apple-jfx-group` is a JavaFX `Group` that is managed by the event
    handler in `handle-apple-change!` so that it contains the graphical
    representation of the (up-to-date) apple state as specified by the
    `ObservableObjectValue` it's listening to.  The graphical representation
    exists within the region `jfx-region` and contains both a dot at the
    (hyperbolic) center of the apple and circle outlining the collision region
    of the apple.  The location of the camera is fixed, i.e. does not follow
    the snake's movement; it stays centered at the origin (in world
    coordinates)"
  [jfx-region]
  (let
    [apple-jfx-group (Group.)
     handle-apple-change!
     (reify ChangeListener
       (changed [this _ _ new-apple]
         (let
           [point-node (make-apple-point-node
                         (::h-center new-apple)
                         jfx-region)
            collision-region-node (make-apple-collision-region-node
                                    (::e-center new-apple)
                                    (::e-radius new-apple)
                                    jfx-region)]
         (.. apple-jfx-group (getChildren) (setAll [point-node
                                                    collision-region-node])))))]
    [apple-jfx-group handle-apple-change!]))

(defn make-view-fixed
  "Returns the pair `[view-jfx-group listeners]`, where:

  * `view-jfx-group` is a JavaFX `Group` containing (newly created) `Node`s
    representing the following elements rendered within the region
    `jfx-region`:
    - The boundary of the disk
    - The fence
    - The apple (point and collision region)
    - The snake (body and arrowhead)

  * `listeners` is a map with entries:

    - `:snake` -> the listener `handle-snake-change!` described in the doc of
      `make-snake-jfx-fixed`
    - `:apple` -> the listener `handle-apple-change!` described in the doc of
      `make-apple-jfx-fixed`.

    The caller will likely want to attach these listeners to the snake state
    and apple state objects."
  [jfx-region]
  (let [disk (make-disk-node jfx-region)
        fence (make-fence-node jfx-region
                               (::fence-e-radius @the-config-derived))
        [snake-jfx-group handle-snake-change!] (make-snake-jfx-fixed
                                                  jfx-region)
        [apple-jfx-group handle-apple-change!] (make-apple-jfx-fixed
                                                  jfx-region)
        view-jfx-group
        (Group. (into-array Node
                            ;; This list determines the z-order, back to front.
                            ;; The disk boundary and the fence are at the back
                            ;; so that they don't obscure the other visual
                            ;; elements.
                            [disk fence
                             apple-jfx-group
                             snake-jfx-group]))]
    [view-jfx-group {:snake handle-snake-change!
                     :apple handle-apple-change!}]))

(defn snake-neck-1p-at-bearing
  "Returns a hyperbolic line segment that determines which camera-shift
  isometry will be used in the follows-the-snake's-head view.  Namely, the
  isometry will carry the snake's head to the endpoint of this segment, and the
  snake's neck (segment containing the head) will coincide, including
  orientation, with the rest of this segment (though not all the way since it
  may be shorter or longer).

  The segment we choose here is the one that puts the snake's head at
  (approximately) the origin and has its head pointing in the direction whose
  absolute angle is `bearing`.  Notes:

  * The snake's head in this view is essentially 'at the origin with jitter';
    the only reason we don't put it exactly at the origin is because that would
    introduce numerics issues: the implementation would then need explicit
    casework on whether a given hyperbolic line is an arc or a diameter
    (limiting case of an arc) of the Poincaré disk.

  * We choose to make the bearing match the fixed-camera view (rather than,
    e.g., having a fixed bearing such as head always facing up) because this is
    less jarring for the player to follow: it makes the surroundings move
    continuously as opposed to, e.g., instantly rotating 90 degrees every time
    the snake's head turns (even though that is what the snake sees)."
  [bearing]
  (let [center (scalev 100.
                       (cossin (+ bearing _halfpi)))
        unnormalized-start-angle (- bearing _halfpi 0.01)
        delta-angle 0.01]
    (as-> (c/h-line center) $
      (assoc $
             ::c/start-angle (residue-just-above unnormalized-start-angle
                                                 (::c/line-angle-min $)
                                                 _2pi)
             ::c/delta-angle delta-angle))))

(s/fdef shift-to-1p :args (s/cat :seg ::c/seg
                                 :bearing number?)
        :ret ::m/complex-2x2)
(defn shift-to-1p
  "Returns the unique isometry of the Poincaré disk that maps:

  * the line containing `seg` to the line containing `snake-neck-1p`,
    preserving orientation; and
  * the endpoint of `seg` to the endpoint of `snake-neck-1p`,

  where `snake-neck-1p := (snake-neck-1p-at-bearing bearing)`.

  This transformation is the backbone of the first-person camera, as applying
  this transformation to the scene at each frame results in the snake's head
  appearing to stay in approximately the same place and facing the direction
  `bearing` (which changes only gradually), with the world (including the rest
  of the snake's body) shifting according to the snake's movements."
  [seg bearing]
  (let [[bwd fwd] (h-line-end-pts-bwd-fwd seg)
        snake-neck (snake-neck-1p-at-bearing bearing)
        [neck-bwd neck-fwd] (h-line-end-pts-bwd-fwd
                              snake-neck)]
    (m/mobius-by-3-pts-R2
      [bwd neck-bwd]
      [fwd neck-fwd]
      [(arc-end-pt seg) (arc-end-pt snake-neck)])))

(s/fdef draw-snake-segs-transformed :args (s/cat :snake-segs ::snake-segs
                                                 :xform ::complex-2x2
                                                 :jfx-region ::jfx-region)
        :ret (s/coll-of ::jfx-node))
(defn draw-snake-segs-transformed
  "Returns a seq of JavaFX `Node`s representing a rendering of a transformed
  version of the snake body `snake-segs` in which all segments have been
  transformed by the isometry `xform`.

  Equivalently, we could say that the snake is unchanged but the camera pose is
  transformed by `(proj-inv-2x2 xform)`."
  [snake-segs xform jfx-region]
  (map #(as-> % $
            (::c/seg $)
            (m/apply-mobius-to-seg xform $)
            (make-seg-node $ jfx-region))
       snake-segs))


(defn make-view-1p
  "Like `make-view-fixed`, but shows a view of the snake from a camera that
  follows the snake's head, so that its head remains visualized at
  (approximately) the center of the disk even as the snake moves.

  That is, returns the pair `[view-jfx-group listeners]`, where:

  * `view-jfx-group` is a JavaFX `Group` containing (newly created) `Node`s
    representing the following elements rendered within the region
    `jfx-region`:
    - The boundary of the disk
    - The fence
    - The apple (point and collision region)
    - The snake (body and arrowhead)

  * `listeners` is a map with the entries:

    - `:snake` -> a `ListChangeListener` that expects to be attached to an
      `ObservableList` of `::snake-segs` (the snake's state).  This listener
      updates all graphical elements that need to be updated when the snake
      moves: the snake body, the apple (point and collision region), and the
      fence.  These all need to be updated each time the snake's head moves
      (which is, every time the snake moves) because the camera also moves
      (i.e., the isometry applied to the underlying state to produce the
      moving-camera view is changed).

    - `:apple` -> A `ChangeListener` that expects to be attached to an
      `ObservableObjectValue` with an `::apple` as its underlying value.  This
      listener updates all the graphical elements that need to be updated when
      the apple changes location: namely, the apple and its collision region.

    The caller will likely want to attach these listeners to the snake state
    and apple state objects."
  [jfx-region get-cur-apple]
  (let [;; --- Visual transform ---
        cur-xform ^{:doc "`cur-xform` is the isometry of the Poincaré disk
                         returned by `shift-to-1p`.  We store it here so that
                         it's available to the listener `handle-apple-change!`
                         which doesn't see the snake directly and needs to know
                         how to transform the apple."}
        (atom (mapv m/to-complex [1 0
                                  0 1]))

        ;; --- Initial states of the graphical elements ---
        disk (make-disk-node jfx-region)
        fence (make-fence-node jfx-region
                               (::fence-e-radius @the-config-derived))
        snake-body-jfx-group (Group.)
        arrowhead (doto (make-arrowhead-node)
                    (move-arrowhead-to-arc-end-pt!
                      (snake-neck-1p-at-bearing _halfpi)
                      jfx-region))
        snake-jfx-group (Group. (into-array Node [snake-body-jfx-group
                                                  arrowhead]))
        apple-jfx-group (Group.)
        snake-and-apple-jfx-group (Group. (into-array Node [snake-jfx-group
                                                            apple-jfx-group]))
        view-jfx-group
        (Group. (into-array Node [disk fence
                                  snake-and-apple-jfx-group]))

        ;; --- Helper functions for the event handlers ---

        update-apple-display!
        ^{:doc "Updates the display of the apple, given its absolute position
               (`new-apple`) and the current visual transform (`xform`).

               This function can/should be called every time either the
               absolute position of the apple changes or the snake's head
               moves, i.e., every game timestep."}
        (fn [new-apple xform]
          (let [transformed-apple (make-apple (m/apply-mobius-R2
                                                xform
                                                (::h-center new-apple)))
                point-node (make-apple-point-node
                             (::h-center transformed-apple)
                             jfx-region)
                collision-region-node (make-apple-collision-region-node
                                        (::e-center transformed-apple)
                                        (::e-radius transformed-apple)
                                        jfx-region)]
            (.. apple-jfx-group
                (getChildren)
                (setAll [point-node
                         collision-region-node]))))

        update-fence!
        ^{:doc "Updates the display of the fence (which does not move).
               This function can/should be called every time the snake's head
               moves, i.e., every game timestep."}
        (fn [xform]
          (let [h-center (m/apply-mobius-R2 xform [0 0])
                [e-center e-radius] (h->e-circle h-center
                                                 (::config/fence-h-radius @the-config))]
            (set-fence-shape! fence e-center e-radius jfx-region)))

        ;; --- JavaFX event handlers ---

        handle-snake-change!
        ^{:doc "Handler for changes to the snake's physical state.
               Updates not just the display of the snake, but also `cur-xform`
               and, therefore, also the display of the apple and fence which
               depend on `cur-xform`."}
        (reify ListChangeListener
          (onChanged [this change]
            (let [head-moved?
                  (loop [has-more? (.next change)]
                    (cond
                      (not has-more?) false
                      (= (.getTo change)
                         (count (.getList change))) true
                      :else (recur (.next change))))]
              (cond
                ; Handle edge case in which the snake body is empty.  At time
                ; of writing, the only time this happens is when the game is
                ; restarted, in `reset-snake!`.
                (empty? (.getList change))
                (.. snake-body-jfx-group
                    (getChildren)
                    (clear))

                head-moved?
                (let [neck-seg (::c/seg (jlist/last (.getList change)))
                      bearing (seg-end-bearing neck-seg)
                      neck-seg-1p (snake-neck-1p-at-bearing bearing)
                      xform (shift-to-1p neck-seg bearing)]
                  (reset! cur-xform xform)
                  (update-fence! xform)
                  (update-apple-display! (get-cur-apple) xform)
                  (.. snake-body-jfx-group
                      (getChildren)
                      (setAll (draw-snake-segs-transformed
                                (.getList change)
                                xform
                                jfx-region)))
                  (move-arrowhead-to-arc-end-pt!
                    arrowhead neck-seg-1p jfx-region))))))

        handle-apple-change!
        ^{:doc "Handler for changes to the apple's physical state (or rather,
               when a new apple appears after the old apple was eaten)."}
        (reify ChangeListener
          (changed [this _ _ new-apple]
            (update-apple-display! new-apple @cur-xform)))

        ;; --- Pieces of the return value ---
        listeners {:snake handle-snake-change!
                   :apple handle-apple-change!}]
    [view-jfx-group listeners]))

(def screen-size-px
  (delay (as-> (Screen/getPrimary) $
           (.getVisualBounds $)
           (min (.getWidth $) (.getHeight $)))))

(defn canvas-size-px-of
  [config]
  (delay (* 1/100
            (::config/canvas-size-percent config)
            @screen-size-px)))

;; ==== Main application ====

(gen-class
  :name snék.game.Application
  :extends javafx.application.Application
  :prefix "snék-")

(defn snék-start [app ^Stage stage]
  (let [;; --- Game state objects ---
        game-start-unix-millis (atom nil)
        game-ts-millis (fn []
                         (- (System/currentTimeMillis)
                            @game-start-unix-millis))
        gen-next-seg-id (let [latest-seg-id (atom 0)]
                          #(swap! latest-seg-id inc))
        snake-segs (FXCollections/observableArrayList)
        num-apples-eaten (atom 0)
        reset-apple! #(change-to-new-apple! % num-apples-eaten
                                            :update-num-fn (fn [_] 0))
        cur-apple (doto (SimpleObjectProperty.)
                    reset-apple!)
        slitherTimeline (Timeline.)

        ;; --- (Re)starting the game ---
        restart-game! (fn []
                        (reset! game-start-unix-millis
                                (System/currentTimeMillis))
                        (reset-snake! snake-segs
                                      (game-ts-millis)
                                      gen-next-seg-id)
                        (reset-apple! cur-apple)
                        (.play slitherTimeline))

        show-game-over-dialog!
        (fn [content-text]
          (doto (Dialog.)
            (.setTitle "Game Over")
            (.setContentText content-text)
            (-> (.getDialogPane)
                (.getButtonTypes)
                (doto
                  (.add (ButtonType. "Restart"
                                     ButtonBar$ButtonData/OTHER))
                  (.add (ButtonType. "Quit"
                                     ButtonBar$ButtonData/CANCEL_CLOSE))))
            (.. (resultProperty)
                (addListener
                  (reify ChangeListener
                    (changed [this prop oldValue newValue]
                      (condp = (.getButtonData newValue)
                        ButtonBar$ButtonData/OTHER (restart-game!)
                        ButtonBar$ButtonData/CANCEL_CLOSE (Platform/exit)
                        (prn "Unknown dialog button:"
                             (.getButtonData newValue)))))))
            (.show)))
        num-apples-text #(str %
                              (if (= % 1) " apple" " apples"))
        handle-game-over! (fn [game-over-reason]
                            (.stop slitherTimeline)
                            (show-game-over-dialog!
                              (str "You ate "
                                   (num-apples-text @num-apples-eaten)
                                   ", good job!  Then, "
                                   game-over-reason)))

        ;; --- Game timestep / slither ---
        slither! (reify EventHandler
                   (handle [this e]
                     (update-snake-head-and-tail!
                       snake-segs
                       (game-ts-millis)
                       (snake-length-after-apples @num-apples-eaten))
                     (if (head-outside-fence? snake-segs)
                       (handle-game-over! "you crashed into the fence."))
                     (if (head-body-crash? snake-segs)
                       (handle-game-over! "you crashed into yourself."))
                     (if (head-near-apple? snake-segs (.getValue cur-apple))
                       (change-to-new-apple!
                         cur-apple num-apples-eaten))))
        handle-key! (reify EventHandler
                      (handle [this e]
                        (if-some [turn-dir ((::config/turn-keycodes @the-config)
                                            (.getCode e))]
                          (let [now-millis (game-ts-millis)]
                            (update-snake-head-and-tail!
                              snake-segs now-millis (snake-length-after-apples
                                                      @num-apples-eaten))
                            (add-snake-seg-from-turn!
                              snake-segs turn-dir now-millis (gen-next-seg-id))))))

        ;; --- Views of the snake in JavaFX ---
        canvas-size-px (canvas-size-px-of @the-config)
        the-main-region (config/make-view-region-main @canvas-size-px)
        the-pip-region (config/make-view-region-pip @canvas-size-px)
        [view-jfx-group-1p listeners-1p] (make-view-1p
                                           the-main-region
                                           #(.getValue cur-apple))
        [view-jfx-group-fixed listeners-fixed] (make-view-fixed
                                                 the-pip-region)
        root (Group. (into-array Node [view-jfx-group-1p
                                       view-jfx-group-fixed
                                       (make-box-around the-pip-region)]))
        scene (doto (Scene. root
                            @canvas-size-px
                            @canvas-size-px
                            Color/WHITE)
                (.addEventHandler KeyEvent/KEY_PRESSED
                                  handle-key!))]

    ;; --- Attach to the event loop ---
    (doto slitherTimeline
      (.. (getKeyFrames)
          (setAll (arr1 KeyFrame
                        (Duration/millis 30)
                        slither!
                        nil)))
      (.setCycleCount Timeline/INDEFINITE))

    (dorun (for [{snake-listener :snake
                  apple-listener :apple} [listeners-fixed
                                          listeners-1p]]
             (do (.addListener snake-segs snake-listener)
                 (.addListener cur-apple apple-listener))))

    ;; --- Create the game window ---
    (doto stage
      (.setScene scene)
      (.setTitle "Snék"))
    (restart-game!)
    (.show stage)))

(defn launch
  "Launch the game, using configuration `config`."
  [config]
  (reset! the-config config)
  (reset! the-config-derived
          (compute-derived-config config))
  (Application/launch
    snék.game.Application
    (into-array String [])))

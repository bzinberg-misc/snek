(ns snék.mobius
  "Utilities for constructing and applying Möbius transformations.

  We represent a Möbius transformation as a 2x2 matrix that stands for an
  element of the projective general linear group PGL(2, ℂ).  This means that,
  for example, the matrices A and 2A represent the same Möbius transformation.
  This matrix acts on the complex projective line by left-multiplication, i.e.
  the point z = [z1 : z2] is mapped to the matrix-vector product Az.
  
  Of particular interest in the hyperbolic plane is the subgroup PSU(1, 1),
  consisting of those Möbius transformations that map the unit circle to itself
  setwise and are orientation-preserving (i.e., they also map the interior of
  the disk to itself).  These are the isometries of the Poincaré disk."
  (:refer-clojure :exclude (+ - * / zero?))
  (:use [clojure.algo.generic.arithmetic :only (+ - * /)]
        [clojure.algo.generic.comparison :only (zero?)]
        [clojure.algo.generic.math-functions :only (abs sqrt)])
  (:import (org.apache.commons.numbers.complex Complex))
  (:require [clojure.spec.alpha :as s]
            [snék.core-util :refer :all]
            [snék.core :as c :refer :all]))

(defmethod + [Complex Complex]
  [x y] (.add x y))

(defmethod - Complex
  [x] (.negate x))

(defmethod - [Complex Complex]
  [x y] (.subtract x y))

(defmethod * [Complex Complex]
  [x y] (.multiply x y))

(defmethod / Complex
  [x] (.divide Complex/ONE x))

(defmethod / [Complex Complex]
  [x y] (.divide x y))

(defmethod zero? Complex
  [z] (.equals z Complex/ZERO))

(defmethod abs Complex
  [z] (.abs z))

(defmethod sqrt Complex
  [z] (.sqrt z))

(s/def ::complex
  ;; Complex number.
  (s/and some?
         #(instance? Complex %)))

(defn complex
  "Returns a complex number with the given real and imaginary parts."
  [real-part imag-part]
  (Complex/ofCartesian real-part imag-part))

(defn R2->complex
  "Like `complex`, but takes a single vector `[real imag]` instead of two
  separate arguments.  Equivalent to `#(apply complex %)`."
  [[real-part imag-part]]
  (complex real-part imag-part))

(defmulti to-complex
  "Widening conversion to `Complex`."
  class)
(defmethod to-complex Complex
  [x] x)
(defmethod to-complex java.lang.Number
  [x] (complex x 0))

(defn to-cartesian
  "Returns the point [Re(z), Im(z)] in R^2."
  [^Complex z]
  [(.real z) (.imag z)])

(defn cis
  "Returns the complex number cos(theta) + i sin(theta)."
  [theta]
  (Complex/ofCis theta))

(defn complex-conj
  "Returns the complex conjugate `a - bi` where `z = a + bi` (a, b real)."
  [z]
  (.conj z))

(s/def ::point-in-CP1
  ;; Point in the complex projective line.  Convention: our default embedding
  ;; of the complex numbers (the affine complex line) into the projective line
  ;; is `z -> [z 1]`.
  (s/coll-of ::complex :count 2))

(s/def ::complex-2x2
  ;; 2x2 matrix of complex numbers, represented as a flat vector in row-major
  ;; order.  That is, the vector `[a b c d]` represents the matrix with rows
  ;; `[a b]` and `[c d]`.
  (s/coll-of ::complex :kind vector? :count 4))

(s/fdef affine->proj :ret ::point-in-CP1)
(defn affine->proj
  "Converts a complex number `x` (either a `::complex` or something that can be
  converted to a `::complex`) to an element of the complex projective line."
  [x]
  (let [z (to-complex x)]
    (if (.isInfinite z)
      [Complex/ONE Complex/ZERO]
      [z Complex/ONE])))

(s/fdef proj->affine :args (s/cat :z ::point-in-CP1)
        :ret ::complex)
(defn proj->affine
  "Converts an element of the complex projective line to a single complex
  number (an element of the complex affine line).  The point of infinity is
  mapped to a value that satisfies `.isInfinite()`.  This is the projective
  inverse of `affine->proj`, in the sense that

      (proj-equiv? z (affine->proj (proj->affine z)))

  and, if `w` is a finite complex number, then

      (= w (proj->affine (affine->proj w)))
  "
  [[z1 z2 :as z]]
  (/ z1 z2))

(s/fdef proj-matmul-2x2
        :args (s/cat :A ::complex-2x2 
                     :B ::complex-2x2)
        :ret ::complex-2x2)
(defn proj-matmul-2x2
  "Returns the product AB.  More precisely, returns a 2x2 matrix belonging to
  the equivalence class of AB in PGL(2)."
  [[a b
    c d :as A]
   [e f
    g h :as B]]
  [(+ (* a e) (* b g))   (+ (* a f) (* b h))
   (+ (* c e) (* d g))   (+ (* c f) (* d h))])

(s/fdef proj-inv-2x2
        :args (s/cat :A ::complex-2x2)
        :ret ::complex-2x2)
(defn proj-inv-2x2
  "Returns the projective inverse of A.  That is, returns a matrix B such
  that AB is a nonzero scalar multiple of the 2x2 identity matrix.  If A is not
  invertible, the return value is unspecified."
  [[a b
    c d :as A]]
  [   d   (- b)
   (- c)     a])

(s/fdef proj-matvecmul-2x2
        :args (s/cat :A ::complex-2x2
                     :v ::point-in-CP1)
        :ret ::point-in-CP1)
(defn proj-matvecmul-2x2
  "Returns the (projective) matrix-vector product Av.  We can view v as an
  element of the projective line ℂP^1, and A acts as a Möbius transformation."
  [[a b
    c d :as A]
   [x y :as v]]
  [(+ (* a x) (* b y))
   (+ (* c x) (* d y))])

(s/fdef mobius-by-0-1-inf
        :args (s/cat :z1 ::complex
                     :z2 ::complex
                     :z3 ::complex)
        :ret ::complex-2x2)
(defn mobius-by-0-1-inf
  "Returns a 2x2 matrix representing (via left-multiplication action) the
  Möbius transformation that carries z1 -> 0, z2 -> 1, and z3 -> inf."
  [z1 z2 z3]
  (let [z2-3 (- z2 z3)
        z2-1 (- z2 z1)]
    [z2-3  (* (- z1) z2-3)
     z2-1  (* (- z3) z2-1)]))

(defn mobius-by-3-pts
  "Returns a 2x2 matrix representing (via left-multiplication action) the
  Mőbius transformation that carries z1 -> w1, z2 -> w2, and z3 -> w3.  Here
  all the z's and w's are finite complex numbers."
  [[z1 w1] [z2 w2] [z3 w3]]
  (proj-matmul-2x2
    (proj-inv-2x2 (mobius-by-0-1-inf w1 w2 w3))
    (mobius-by-0-1-inf z1 z2 z3)))

(defn mobius-by-3-pts-R2
  "Like `mobius-by-3-pts` but the values `z_i` and `w_i` are represented as
  pairs of real numbers rather than complex numbers."
  [& args]
  (apply mobius-by-3-pts (map #(map R2->complex %)
                              args)))

(s/fdef apply-mobius-R2 :args (s/cat :xform ::complex-2x2
                                     :pt ::c/point2d))
(defn apply-mobius-R2
  "Applies the Mőbius transformation `xform` to the point `pt`, returning a
  point in R^2.

  Here `pt`, a vector `[a, b]` of two real numbers, is identified with the
  complex number `z := a + bi`.  The return value is similarly represented as
  the vector `[Re(xform(z)), Im(xform(z))]`."
  [xform pt]
  (->> pt
       (apply complex)
       (affine->proj)
       (proj-matvecmul-2x2 xform)
       (proj->affine)
       (to-cartesian)))

(s/fdef apply-mobius-to-seg :args (s/cat :xform ::complex-2x2
                                         :seg ::c/seg))
(defn apply-mobius-to-seg
  "Applies the Möbius transformation `xform` to `seg`, returning a new `::seg`.

  It as assumed that `xform` is an isometry of the Poincaré disk, and this is
  what allows us to claim that the returned value satisfies the constraints
  required by `::seg` (being perpendicular to the boundary of the disk, etc.)."
  [xform seg]
  (let [[old-end-pt-bwd old-end-pt-fwd
         :as old-end-pts] (h-line-end-pts-bwd-fwd seg)
        {old-center ::c/center
         old-radius ::c/radius}  seg
        old-radius-v (-v old-end-pt-bwd old-center)
        old-antipodal-pt (-v old-center old-radius-v)

        [end-pt-bwd end-pt-fwd _
         :as three-pts-on-new-circle] (for [pt (conj (vec old-end-pts)
                                                     old-antipodal-pt)]
                                        (apply-mobius-R2 xform pt))
        center (apply circum-center three-pts-on-new-circle)
        radius (e-dist center end-pt-fwd)
        [line-angle-min
         line-angle-max]  (apply rerep-and-order-line-angle-extremes
                                 (map #(angle-of (-v % center))
                                      [end-pt-bwd end-pt-fwd]))

        [seg-start-angle
         seg-end-angle] (for [old-angle [(::c/start-angle seg)
                                         (+ (::c/start-angle seg)
                                            (::c/delta-angle seg))]]
                          (let [old-pt (point-on-circle old-center
                                                        old-radius
                                                        old-angle)
                                new-pt (apply-mobius-R2 xform old-pt)]
                            (residue-just-above
                              (angle-of (-v new-pt center))
                              line-angle-min
                              _2pi)))]
    (as-> {::c/center center
           ::c/radius radius
           ::c/line-angle-min line-angle-min
           ::c/line-angle-max line-angle-max
           ::c/start-angle seg-start-angle
           ::c/delta-angle (- seg-end-angle seg-start-angle)}
      $
      (assoc $ ::c/h-length (h-length-of $)))))


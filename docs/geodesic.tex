\documentclass[11pt]{article}

\usepackage{amsmath,amssymb,amsthm}
\usepackage[inline]{asymptote}
\usepackage[T1]{fontenc}
\usepackage{fouriernc}
\usepackage{fullpage}
\usepackage{comment}

\title{Drawing geodesics in the Poincar\'e disk}
\author{Ben Zinberg}
\date{April 2022}

\newcommand{\pn}[1]{\left( #1 \right)}
\newcommand{\abs}[1]{\left\lvert #1 \right\rvert}
\newcommand{\aabs}[1]{\left\lVert #1 \right\rVert}
\newcommand{\smabs}[1]{\lvert #1 \rvert}
\newcommand{\smaabs}[1]{\lVert #1 \rVert}
\newcommand{\R}{\mathbb{R}}
\newcommand{\qtext}[1]{\quad\text{#1}\quad}

\begin{document}

\maketitle

\section{Summary}

These notes derive some of the geometric formulas used to move the snake around
the Poincar\'e disk.

\section{Walking along geodesics}

In this section we ask: Given a point $P$ inside the Poincar\'e disk, a
direction $v$, and a real number $d > 0$, what is the point $d$ units away from
$P$ in the direction $v$?  If we think of the Poincar\'e disk as a Riemannian
manifold, this is the same as asking how to compute the exponential map.
However, due to the nice geometry of the hyperbolic plane, we don't need to
work at that level of abstraction.  Instead of working with an abstract tangent
vector $v \in T_P(D^2)$, we can just ``draw'' the full geodesic through $P$ in
a particular direction and solve for the desired point (call it $Q$).  In fact,
we can use ``choice of geodesic through $P$ along with a direction (forward or
backward) along that geodesic'' as our concrete representation of tangent
vectors at $P$.  The geodesics in the Poincar\'e disk are Euclidean arcs that
meet the disk at a right angle, so even more concretely, we can represent the
tangent vector as ``Euclidean center of that arc, and sign ($+1$ or $-1$).''
Here a sign of $+1$ (resp., $-1$) means the direction of increasing (resp.,
decreasing) central angle in the Euclidean arc.\footnote{
    It's tempting to call these directions ``counterclockwise'' and
    ``clockwise,'' but based on some experience writing geometry software that
    has to work with different rendering backends, I think it's best not to.
    The problem is that that terminology commits to a right-handed coordinate
    system, whereas several popular rendering engines (including Unity and
    UnrealEngine) default to a left-handed coordinate system.
}

The picture looks like this:

\begin{center}
    \asyinclude[width=7cm]{sketch.asy}
\end{center}

Here the Poincar\'e disk $\mathcal{D}$ is the unit circle in $\R^2$, centered
at the origin $O$.  The point $P$ is given, and the Euclidean center $O'$ of a
particular geodesic through $P$ is also given.  A real number $d > 0$ is given,
and we want to compute the point $Q$ on that geodesic at distance $d$ from $P$.
Let's assume for now that the direction is positive, i.e. increasing central
angle; the negative case is similar and we'll cover it briefly below.

Let $A$ and $B$ be the ideal points at the end of the geodesic, ordered so that
the direction in which we want to draw the geodesic points from $A$ to $B$.
Let $\omega$ be half the central angle of Euclidean arc $AB$, and let $\alpha,
\gamma, \beta$ be half the central angles of $AP$, $PQ$ and $QB$ respectively.
Thus $\omega = \alpha + \gamma + \beta$.

Since $OAO'$ is a right triangle, we have
\[ \sin\omega = \frac{1}{\abs{OO'}}, \]
where $\smabs{OO'}$ is the Euclidean length of the Euclidean line segment $OO'$.
And since $O'AQ$ is isosceles, we have
\[ \sin\alpha = \frac{\abs{AP}}{2r}, \]
where $r := \smabs{O'A}$ is the Euclidean radius of the geodesic.  To find $Q$,
we need to solve for $\gamma$.

By definition,
\[ d = \log \frac{
    \abs{AQ} \cdot \abs{BP}
}{
    \abs{AP} \cdot \abs{BQ}
}. \]
Letting $D := \exp(d)$, we have
\[ D = \frac{
    \abs{AQ} \cdot \abs{BP}
}{
    \abs{AP} \cdot \abs{BQ}
}. \]
Trigonometry gives $\smabs{AQ} = 2r \sin(\alpha + \gamma)$ and so on, and the
above reduces to
\begin{align*}
    D
    &= \frac{
        \sin(\alpha + \gamma)\ \sin(\gamma + \beta)
    }{
        \sin(\alpha)\ \sin(\beta)
    } \\[2mm]
    &= \frac{
        \sin(\alpha + \gamma)\ \sin(\omega - \alpha)
    }{
        \sin(\alpha)\ \sin(\omega - \alpha - \gamma)
    } \\[2mm]
    &= \frac{
        \sin\eta\ \sin(\omega - \alpha)
    }{
        \sin(\alpha)\ \sin(\omega - \eta)
    },
\end{align*}
where $\eta := \alpha + \gamma$.  Rearranging, we have
\[
    \frac{\sin\alpha}{\sin(\omega - \alpha)}
    D
    =
    \frac{
        \sin\eta
    }{
        \sin(\omega - \eta)
    }.
\]
Trig identities then give
\begin{align*}
    \frac{\sin\alpha}{\sin(\omega - \alpha)}
    D
    &=
    \frac{
        \sin\eta
    }{
        \sin\omega\ \cos\eta - \cos\omega\ \sin\eta
    } \\[2mm]
    &=
    \frac{
        \tan\eta
    }{
        \sin\omega - \cos\omega\ \tan\eta
    }.
\end{align*}
Let $b := \frac{\sin\alpha}{\sin(\omega-\alpha)}D$ denote the left hand side,
and notice that the right hand side is a fractional-linear transformation of
the unknown $\tan\eta$.  Applying its inverse, we have
\[
    \tan\eta = \frac{c_1 b + c_2}{c_3 b + c_4},
    \quad\text{where}\quad
    \begin{pmatrix} c_1 & c_2 \\ c_3 & c_4 \end{pmatrix}
    \propto
    \begin{pmatrix}
        1 & 0 \\
        -\cos\omega & \sin\omega
    \end{pmatrix}^{-1}.
\]
Here ``$\propto$'' means ``is any scalar multiple of.''  In particular we can
take
\[
    \begin{pmatrix} c_1 & c_2 \\ c_3 & c_4 \end{pmatrix}
    =
    \begin{pmatrix}
        \sin\omega & 0 \\
        \cos\omega & 1
    \end{pmatrix},
\]
yielding
\[
    \tan\eta = \frac{b \sin\omega}{1 + b \cos\omega}.
\]
By construction we have $0 \leq \eta < \frac{\pi}{2}$, so
\[
    \eta = \arctan\pn{\frac{b \sin\omega}{1 + b \cos\omega}}
\]
and $\gamma = \eta - \alpha$.  Note also that owing to the geometry of the
figure, we have
\[
    \frac{\sin\alpha}{\sin(\omega - \alpha)} = \frac{\abs{AP}}{\abs{BP}}.
\]
Thus
\[
    b = \frac{\abs{AP}}{\abs{BP}} D.
\]
Also, we have $\sin\omega = \frac{1}{\abs{OO'}}$ and $0 \leq \omega <
\frac{\pi}{2}$, so $\cos\omega = \sqrt{1 - \frac{1}{\abs{OO'}^2}}$ and
\begin{align*}
    \eta
    &=
    \arctan\pn{
        \frac{
            \frac{1}{\abs{OO'}} b
        }{
            \pn{\sqrt{1 - \frac{1}{\abs{OO'}^2}}} b + 1
        }
    } \\[2mm]
    &=
    \arctan\pn{
        \frac{
            b
        }{
            \pn{\sqrt{\abs{OO'}^2 - 1}} b + \abs{OO'}
        }
    } \qquad\text{(positive case)}.
\end{align*}

In the case where the direction of the extension is negative (decreasing
central angle), we can employ an almost identical mirror argument by swapping
$A$ and $B$ and replacing $\alpha$, $\gamma$ and $\beta$ with their negatives:

\begin{center}
    \asyinclude[width=7cm]{sketch_mirror.asy}
\end{center}

We again get $\gamma = \eta - \alpha$, but here $\gamma$, $\eta$ and $\alpha$
are all negative.  With $b$ the same as above, the formula for $\eta$ picks up
a minus sign:

\[
    \eta
    =
    \arctan\pn{
        \frac{
            -b
        }{
            \pn{\sqrt{\abs{OO'}^2 - 1}} b + \abs{OO'}
        }
    } \qquad\text{(negative case)}.
\]
We can of course combine the positive and negative cases into the single formula
\[
    \eta
    =
    \arctan\pn{
        \frac{
            \mathit{dir} \cdot b
        }{
            \pn{\sqrt{\abs{OO'}^2 - 1}} b + \abs{OO'}
        }
    },
\]
where $\mathit{dir} \in \{\pm 1\}$ is the desired direction of the arc.

\section{Turning}

By ``turning,'' we mean finding---say, determining the center of---the geodesic
through a given point $P$ whose radius vector to $P$ is a scalar multiple of a
given nonzero vector $\vec{u} = (u, v) \in \R^2$.  For now we'll assume
$\vec{u}$ is a unit vector, but in the end we'll see that the formula for the
center is invariant under scaling $\vec{u}$, so that assumption is not
necessary.

Let $O'$ denote the center of the unknown geodesic and $r$ its radius.  Since
$O'PO$ is a right triangle, we have
\[ r^2 + 1 = \abs{O'O}^2.  \]
Working in Euclidean coordinates with the center of the disk $O$ at the origin,
let $P = (x, y)$.  Then we have
\[ O' = (x - ur,\ y - vr). \]
Combining the above two equations, we have
\begin{align*}
    r^2 + 1
    &= (x - ur)^2 + (y - vr)^2 \\
    &= \pn{u^2 + v^2}r^2 - 2\pn{ux + vy}r + \pn{x^2 + y^2}.
\end{align*}
Seeing as $u^2 + v^2 = 1$, the $r^2$ terms cancel and we get
\begin{align*}
    r
    &= \frac{x^2 + y^2 - 1}{2\pn{ux + vy}} \\
    &= \frac{\abs{P}^2 - 1}{2\pn{P \cdot \vec{u}}}
\end{align*}
(where here ``$P$'' really denotes the vector from $O$ to $P$), and
\[ O' = P - r\vec{u}. \]
Note that this formula for $O'$ is is unchanged if we drop the assumption that
$\vec{u}$ is a unit vector: if we take $\vec{u} \gets a\vec{u}$, that gives $r
\gets r/a$ and so $r\vec{u}$ is unchanged (though in this case $r$ is no longer
the radius of the arc).

\end{document}

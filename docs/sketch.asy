pair O = (0, 0);
pair A = O + unit((-1, -5));
pair Oprime = A + scale(1.2) * rotate(-90) * (A - O);

real omega = asin(1 / length(Oprime - O));
real alpha = 0.23 * omega;
real gamma = 0.34 * omega;
real beta = omega - alpha - gamma;

pair B = Oprime + rotate(2 * omega * 180/pi) * (A - Oprime);
pair P = Oprime + rotate(2 * alpha * 180/pi) * (A - Oprime);
pair Q = Oprime + rotate(2 * gamma * 180/pi) * (P - Oprime);

real r = length(A - Oprime);
draw(circle(O, 1));
draw(arc(Oprime, r, 180/pi * angle(A - Oprime), 180/pi * angle(B - Oprime)));
dot(A);
dot(B);
dot(P);
dot(Q);
dot(Oprime);
dot(O);

draw(Oprime--A, blue);
draw(Oprime--B, blue);
draw(Oprime--P, blue);
draw(Oprime--Q, blue);

draw(Oprime--O--A, dashed + orange);
draw(shift(A)
     * rotate(180/pi * angle(A - Oprime))
     * scale(0.05)
     * ((-1, 0)--(-1,1)--(0,1)),
     orange);

label("$\mathcal{D}$", (0, 1), NE);
label("$A$", A, S);
label("$B$", B, N);
label("$P$", P, NW);
label("$Q$", Q, W);
label("$O$", O, E);
label("$O'$", Oprime, SW);

label("$2\alpha$", Oprime, 8 * unit(A + P - 2 * Oprime), blue + fontsize(9));
label("$2\gamma$", Oprime, 6 * unit(P + Q - 2 * Oprime), blue + fontsize(9));
label("$2\beta$", Oprime, 5 * unit(Q + B - 2 * Oprime), blue + fontsize(9));

(ns build
  (:require [clojure.tools.build.api :as b]))

; TODO: Add this package to a Maven package registry (perhaps GitLab's)
(def lib 'com.example/snék)
(def version "2.2")
(def src-dirs ["src"])
(def class-dir "target/classes")
(def uber-file (str "target/" (name lib) "-" version "-standalone.jar"))
(def main-class 'snék.main)

(def basis (delay (b/create-basis {:project "deps.edn"})))

(defn clean [_]
  (b/delete {:path "target"}))

(defn compile-clj [_]
  (b/compile-clj {:basis @basis
                  :src-dirs src-dirs
                  :class-dir class-dir}))

(defn uber [_]
  (clean nil)
  (b/copy-dir {:src-dirs src-dirs
               :target-dir class-dir})
  (compile-clj nil)
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis @basis
           :main main-class}))

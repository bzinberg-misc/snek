# snék

<div align="center">

![Gameplay gif](gameplay.gif)
</div>

Snék is a
[snake game](https://en.wikipedia.org/wiki/Snake_%28video_game_genre%29)
that takes place in the
[Poincaré disk](https://en.wikipedia.org/wiki/Poincar%C3%A9_disk_model).
As a hungry snake, your goal is to eat as many apples as possible without
crashing into the fence or into yourself.

## Game view

Two views are shown simultaneously:

* A larger, first-person view in which the camera follows the snake's head
* A smaller, picture-in-picture in the bottom-right corner, in which the camera
  stays fixed.

## Controls

Use the left and right arrow keys to turn 90 degrees in either direction.
There is no need for the up and down keys.  Note that these turn directions are
relative to the snake's current heading, not the application window.

## It's hyperbolic

The fence, represented by the brown circle, has radius approximately 2.944.
(Unlike Euclidean space, in the hyperbolic plane there is a canonical choice of
length unit, namely, the one that makes the curvature of the plane equal to
$`-1`$.)

The dashed black circle represents the edge of the world -- informally
speaking, a circle of infinite radius.  More formally, it is the boundary
consisting of so-called
[points at infinity](https://en.wikipedia.org/wiki/Point_at_infinity).

For Euclidean beings such as ourselves (ok fine,
[it's complicated](https://en.wikipedia.org/w/index.php?title=Shape_of_the_universe&oldid=1200711910#Curvature_of_the_universe)),
the first-person view appears as if the world is shifting as the snake moves
around.  But it's not: the fence isn't moving, only you are.

## How to launch

### Option 1: Download and click

Click through to the latest
[release](https://gitlab.com/bzinberg-misc/snek/-/releases)
and download the attached Uberjar (standalone Java executable).  Assuming you
have a Java Runtime Environment installed, and assuming you trust this software
enough to run it on your computer, you can run the game by double-clicking the
Uberjar.

### Option 2: Run from the Clojure command line

To run the game from the Clojure command line, open a shell at the root
directory of this repo.  Before the first time you run the game, you will need
to compile it:

```shell
clj -T:build compile-clj
```

Then, you can launch the game by running

```shell
clj -M -m snék.main
```

## Using a custom configuration

You can supply a config file from the command line.  To create the config
file, start with the template [`config_example.edn`](config_example.edn) and
uncomment any of the entries for which you want to supply a custom value.  Then
give the path to that file as the command-line argument `--config-file`, e.g.
```shell
java -jar /path/to/snék-X.Y-standalone.jar --config-file /path/to/my_config.edn
```
or, if running from the Clojure source,
```shell
clj -T:build compile-clj
clj -M -m snék.main --config-file /path/to/my_config.edn
```

## Math derivations

The more involved geometry calculations used in this game are explained in
[`docs/geodesic.pdf`](docs/geodesic.pdf).  The code is also well documented via
docstrings and comments, including math explanations in a few places such as
[here](https://gitlab.com/bzinberg-misc/snek/-/blob/8fb98b67c3fedccc4adff54a83b7b26ba36c3034/src/sn%C3%A9k/core.clj#L537-569).

## Where this came from

This is my first project in Clojure.  The idea of writing a snake game in
Clojure came from
[_Programming Clojure 3e_](https://pragprog.com/titles/shcloj3/programming-clojure-third-edition/)
by Alex Miller, an introduction I enjoyed very much and would recommend.
